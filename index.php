<?php 
session_start();
if(!isset($_SESSION['name'])){
		header('location:login.php');
}

$page_ar = explode(',', $_SESSION['pages']);
$action_ar = explode(',', $_SESSION['action']);		
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta http-equiv="refresh" content="300;url=backend/logout.php" />
  <title>SAMBANDH</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Dashboard </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard </li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row">
        
          <?php if(in_array('Add',$action_ar) && in_array('Add_Member',$page_ar)){ ?>
		   <div class="col-12 col-sm-6 col-md-6">
		  
		   <a href="add_parent.php" style="color:black;">
            <div class="info-box mb-3">
              <span class="info-box-icon bg-warning elevation-1"><i class="	fa fa-user-plus"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">Add New Sainik</span>
                <span class="info-box-number"></span>
              </div>
			 
              <!-- /.info-box-content -->
            </div> </a>
            <!-- /.info-box -->
          </div>
		  <?php } ?>
		  <?php if(in_array('View',$action_ar) && in_array('View_Member',$page_ar)){ ?>
		   <div class="col-12 col-sm-6 col-md-6">
		    <a href="view_members_grid.php" style="color:black;">
            <div class="info-box mb-3">
			
              <span class="info-box-icon bg-warning elevation-1"><i class="fas fa-users"></i></span>

              <div class="info-box-content">
                <span class="info-box-text">View Sainik</span>
                <span class="info-box-number" id="total_members"></span>
              </div>
              <!-- /.info-box-content -->
            </div></a>
            <!-- /.info-box -->
          </div>
		   <?php } ?>
        <div class="container-fluid">
		<div class="row">
          <div class="col-12">
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">Recent Sainik </h3>

               <!-- <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>!-->
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
					 <th></th>
                      <th>Army no</th>
					  <th>Rank</th>
                      <th>Name</th>
                      <th>Cas Date</th>
					  <th>Serving Unit</th>
					  <th>Parent Unit</th>
					  
                    </tr>
                  </thead>
                  <tbody id="table_data_recent_members">
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          </div>
          </div>
		   <div class="container-fluid">
		   <?php if($_SESSION['role']=='super admin' || $_SESSION['role']=='admin'){?>
		<div class="row">
          <div class="col-12">
			<div class="card">
              <div class="card-header" style="background: #FF9800;color: white;">
                <h3 class="card-title">Login Track </h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="DATE" name="table_search" class="form-control float-right" placeholder="Search" 
					onchange="date_wise_login_track(this.value)">
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>S.No</th>
					  <th>User name</th>
					  <th>IP address</th>
                      <th>Login time</th>
                      <th>Logout time</th>
					  <th>Status</th>
					  
                    </tr>
                  </thead>
                  <tbody id="table_data_login_track">
				 
                  </tbody>
                </table>
				
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          </div>
		  <?php }?>
		  
          </div>
          <!-- /.col -->
        </div>
       
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

 <?php include('footer.php'); ?>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="dist/js/pages/dashboard2.js"></script>
<script>
$( document ).ready(function() {
	
	view_recent_members();
	total_count_members();
	login_track();
});
function view_recent_members(){
	
	var dataString = 'type=view_recent_members';
    $.ajax({
		url:'backend/dashboard.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#table_data_recent_members').html(data);
				
		}
	});
}
function total_count_members(){
	var dataString = 'type=total_count_members';
    $.ajax({
		url:'backend/dashboard.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			
				$('#total_members').html('( '+data+')');
				
		}
	});
}
function login_track(){
	var dataString = 'type=login_track';
    $.ajax({
		url:'backend/dashboard.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			
				$('#table_data_login_track').html('( '+data+')');
				
		}
	});
}
function date_wise_login_track(date){
	var dataString = 'type=login_track_basedon_date&date='+date;
    $.ajax({
		url:'backend/dashboard.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			
				$('#table_data_login_track').html('( '+data+')');
				
		}
	});
}
</script>
</body>
</html>
