<?php 
session_start();
INCLUDE('config.php');
// if(!isset($_SESSION['name'])){
		// header('location:login.php');
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">

  <title>SAMBATH</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <style>
.alert {
 width: 96%;
    padding: 1px;
    background-color: #f44336;
    color: white;
    opacity: 1;
    transition: opacity 0.6s;
    margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}

</style>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add / View Roles </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">add Role </li>
            </ol>
          </div><!-- /.col -->
		  <div class="alert success" style="display:none;">
			  <span class="closebtn">&times;</span>  
			  <strong>Success!</strong> <span id="success"></span>
			</div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row"> 
		   <div class="col-12 col-sm-6 col-md-6">
			<div class="card">
				<div class="card-body register-card-body">
				  <p class="login-box-msg">Add a new Role</p>
					 <div class="input-group mb-3">
					  <input type="hidden" name="table_id" id="table_id">
					  <input type="text" name="role" id="role" required class="form-control" placeholder="Role">
					  <div class="input-group-append">
						<div class="input-group-text">
						  <span class="fas fa-user"></span>
						</div>
					  </div>
					</div>
					<div class="input-group mb-3" style="display: inherit;">
					<label>Choose pages</label><br>
					 <input type="checkbox" value="Dashboard" id="Dashboard" name="pages"> <span> Dashboard</span><br>
					 <input type="checkbox" value="Add_Sainik" id="Add_Sainik" name="pages"> Add  Sainik<br>
					 <input type="checkbox" value="View_Sainik" id="View_Sainik" name="pages"> View  Sainik<br>
					 <input type="checkbox" value="Roles" id="Roles" name="pages"> Roles <br>
					 <input type="checkbox" value="Saathi" id="Saathi" name="pages"> Saathi<br>
					 <input type="checkbox" value="Reports" id="Reports" name="pages"> Reports <br>
					 <input type="checkbox" value="Bulk_sainik_upload" id="Reports" name="pages"> Bulk Sainik Upload <br>
					
					 
					</div>
					<div class="input-group mb-3" style="display: inherit;">
					<label>Choose Action</label><br>
					 <input type="checkbox" value="Add" id="Add" name="Action"> <span>Add</span><br>
					 <input type="checkbox" value="View" id="View" name="Action"> <span>View</span><br>
					 <input type="checkbox" value="Edit" id="Edit" name="Action"> Edit<br>
					 <input type="checkbox" value="Delete" id="Delete" name="Action"> Delete<br>
					 <input type="checkbox" value="Download" id="Download" name="Action"> Download<br>
					</div>
					
					<div class="row">
					  <div class="col-8">
					  
					  </div>
					  <!-- /.col -->
					  <div class="col-4">
						<button type="submit" name="submit" id="add" onclick="add_role()" class="btn btn-primary btn-block">Add</button>
						<button style="display:none;" type="submit" name="submit" id="update" onclick="update_role()" class="btn btn-primary btn-block">Update</button>
					  </div>
					  <!-- /.col -->
					</div>
				 

				</div>
   
			</div>
		</div>
        <div class="container-fluid">
		<div class="row">
          <div class="col-12">
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">Roles List </h3>

                <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap">
                  <thead>
                    <tr>
                      <th>S.no</th>
                      <th>Role</th>
					  <th>Pages</th>
					  <th>Privilege</th>
					  <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="table_data">
					
					 
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        
        <!-- Main row -->
       

            <!-- PRODUCT LIST -->
           
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

 <?php include('footer.php'); ?>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>

<!-- PAGE SCRIPTS -->
<script src="dist/js/pages/dashboard2.js"></script>
<script>
$( document ).ready(function() {
	
	view_roles();
});
function view_roles(){
	
	var dataString = 'type=view_roles';
    $.ajax({
		url:'backend/add_update_roles.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#table_data').html(data);
				
		}
	});
}
function add_role(){	
	var pages=$('input[name=pages]:checked').map(function() {
    return this.value;
	}).get();
	var Action=$('input[name=Action]:checked').map(function() {
    return this.value;
	}).get();
	
	var dataString = 'role='+ $('#role').val() + '&pages='+ pages+'&type=add_role&action='+Action;

	$.ajax({
		url:'backend/add_update_roles.php',
		type: 'POST',
        data:dataString,
		success: function(message){
			
			if(message==1){
				$('#role').val('');
				$('input[type=checkbox]').prop("checked", false);
				$('#success').val('Successfully Added');
				$('.alert').css('display','inline');
				view_roles();
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
		}
	});
}
function edit_role(id){
	var dataString = 'id='+id+ '&type=edit_role';
	$.ajax({
		url:'backend/add_update_roles.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
			    $('input[type=checkbox]').prop("checked", false);
				$('#add').css('display','none');
				$('#update').css('display','inline');
				var obj = jQuery.parseJSON(data);
				$('#table_id').val(obj[0].id);
				$('#role').val(obj[0].role);
				var pages=obj[0].pages.split(',');
				$.each(pages, function(index, value) { 
				  $('#'+value).prop("checked", true);	
				});
				var action=obj[0].action.split(',');
				$.each(action, function(index1, value1) { 
				
				  $('#'+value1).prop("checked", true);	
				});
				
				
		}
	});
}
function update_role(){
	
	var pages=$('input[name=pages]:checked').map(function() {
    return this.value;
	}).get();
	var Action=$('input[name=Action]:checked').map(function() {
    return this.value;
	}).get();
	

	var dataString = 'id='+ $('#table_id').val()+'&role='+ $('#role').val() + '&pages='+ pages+'&type=update_role&action='+Action;
	$.ajax({
		url:'backend/add_update_roles.php',
		type: 'POST',
        data:dataString,
		success: function(message){
			
			if(message==1){
				$('#role').val('');
				$('input[type=checkbox]').prop("checked", false);
				$('#success').val('Successfully updated record');
				$('.alert').css('display','inline');
				view_roles();
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
		}
	});
}
function delete_role(id){
	if(confirm('Are you sure Want to Delete the selected Record')==false){
		return false;
	}
	var dataString = 'id='+id+ '&type=delete_role';
	$.ajax({
		url:'backend/add_update_roles.php',
		type: 'POST',
        data:dataString,
		success: function(message){	
				if(message==1){
				$('#role').val('');
				$('input[type=checkbox]').prop("checked", false);
				$('#success').val('Successfully deleted');
				$('.alert').css('display','inline');
				view_roles();
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
		}
	});
}
</script>
</body>
</html>
