<!DOCTYPE html>
<html>
<?php INCLUDE('head.php');?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add Account</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add Account</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Account</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form">
                <div class="card-body">  
				  <div class="row">
				   <div class="col-md-3">
						<div class="form-group">
							<label for="">Army no</label>
							<select class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
								<option selected="selected">Ar123</option>
								<option>Ae234</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Name of NOK</label>
							<input type="text" class="form-control" id="" placeholder="Enter Name of NOK">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">NOK GPS pin</label>
							<input type="number" class="form-control" id="" placeholder="Enter GPS">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">NOK Telephone</label>
							<input type="text" class="form-control" id="" placeholder="Enter Name of NOK">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">PPO</label>
							<input type="text" class="form-control" id="" placeholder="Enter PPO">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">PDA</label>
							<input type="text" class="form-control" id="" placeholder="Enter PDA">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">PDA Details</label>
							<input type="text" class="form-control" id="" placeholder="Enter PDA Details">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Pension Bank</label>
							<input type="text" class="form-control" id="" placeholder="Pension Bank">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Pension Bank Branch</label>
							<input type="text" class="form-control" id="" placeholder="Enter Pension Bank Branch">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Bank Account Number</label>
							<input type="text" class="form-control" id="" placeholder="Enter Bank Account Number">
						</div>
					</div>
					
					
				  </div>
				  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <!-- /.card -->

          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php include('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>

<script src="dist/js/adminlte.min.js"></script>

<script src="dist/js/demo.js"></script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
</script>
</body>
</html>
