<?php 
SESSION_START();
	INCLUDE('config.php');
// if(!isset($_SESSION['name'])){
		// header('location:login.php');
// }
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <meta http-equiv="x-ua-compatible" content="ie=edge">
   <meta http-equiv="refresh" content="3600;url=backend/logout.php" />

  <title>SAMBATH</title>

  <!-- Font Awesome Icons -->
  <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
  <!-- overlayScrollbars -->
  <link rel="stylesheet" href="plugins/overlayScrollbars/css/OverlayScrollbars.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/adminlte.min.css">
  <!-- Google Font: Source Sans Pro -->
  <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
  <link href="https://cdn.datatables.net/1.10.21/css/jquery.dataTables.min.css" rel="stylesheet">
  <script src="https://cdn.datatables.net/1.10.21/js/jquery.dataTables.min.js"></script>
  <script >
  $(document).ready(function() {
    $('#example').DataTable();
} );
  </script>
   <style>
.alert {
 width: 96%;
    padding: 1px;
    background-color: #f44336;
    color: white;
    opacity: 1;
    transition: opacity 0.6s;
    margin-bottom: 15px;
}

.alert.success {background-color: #4CAF50;}
.alert.info {background-color: #2196F3;}
.alert.warning {background-color: #ff9800;}

.closebtn {
  margin-left: 15px;
  color: white;
  font-weight: bold;
  float: right;
  font-size: 22px;
  line-height: 20px;
  cursor: pointer;
  transition: 0.3s;
}

.closebtn:hover {
  color: black;
}

</style>
</head>
<body class="hold-transition sidebar-mini layout-fixed layout-navbar-fixed layout-footer-fixed">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1 class="m-0 text-dark">Add / View Saathi </h1>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">add users </li>
            </ol>
          </div><!-- /.col -->
		  <div class="alert success" style="display:none;">
			  <span class="closebtn">&times;</span>  
			  <strong>Success!</strong> <span id="success"></span>
			</div>
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Info boxes -->
        <div class="row"> 
		   <div class="col-12 col-sm-6 col-md-6">
			<div class="card">
				<div class="card-body register-card-body">
				  <p class="login-box-msg">Register a new User</p>
				     <form action="" name="user_form">
					<div class="input-group mb-3">
					 <input type="hidden" name="table_id" id="table_id">
					  <input type="text" class="form-control" name="name" id="name" placeholder="Full name">
					  <div class="input-group-append">
						<div class="input-group-text">
						  <span class="fas fa-user"></span>
						</div>
					  </div>
					</div>
					<div class="input-group mb-3">
					  <input type="text" class="form-control" name="user_name" id="user_name" placeholder="User name">
					  <div class="input-group-append">
						<div class="input-group-text">
						  <span class="fas fa-user"></span>
						</div>
					  </div>
					</div>
					<div class="input-group mb-3">
					 <label id="error_mobile" style="color:red;width: 100%;display:none;"></label>
					  <input type="number" class="form-control" onkeyup="mobile_error()" name="user_mobile" id="user_mobile" placeholder="Mobile">
					  <div class="input-group-append">
						<div class="input-group-text">
						  <span class="fas fa-phone"></span>
						</div>
					  </div>
					</div>
					<div class="input-group mb-3">
					  <SELECT class="form-control" name="role" id="role">
						<OPTION value="0" id="default">Select Role</OPTION>
						<?php
								$qry="SELECT role FROM `sambndh_role` ORDER BY role ASC";
								$run=mysqli_query($con,$qry);
								while($result=mysqli_fetch_assoc($run)){
						?>
						<OPTION id="i<?php echo $result['role']; ?>" value="<?php echo $result['role']; ?>"><?php echo $result['role']; ?></OPTION>
						<?php } ?>
					  </SELECT>
					</div>
					<div class="input-group mb-3 password">
					 <label id="pass_len" style="color:red;width: 100%;display:none;">** minimum 6 character</label>
					  <input type="password" name="password" id="password" onkeyup="pass_len()" class="form-control" placeholder="Password">
					  <div class="input-group-append">
						<div class="input-group-text">
						  <span class="fas fa-lock"></span>
						</div>
					  </div>
					</div>
					<div class="input-group mb-3 update_password" style="display:none;">
					 <label id="pass_len_update" style="color:red;width: 100%;display:none;">** password minimum 6 character</label>
					  <input  type="password" name="update_password" id="update_password" class="form-control" onkeyup="pass_len_update()" placeholder="password">
					
					</div>
					<div class="input-group mb-3 cpassword">
					  <label id="pass_error" style="color:red;width: 100%;display:none;">** password not matched with above</label>
					  <input onkeyup="cpassword1()" type="password" name="cpassword" id="cpassword" class="form-control" placeholder="Retype password">
					  <div class="input-group-append">
						<div class="input-group-text">
						  <span class="fas fa-lock"></span>
						</div>
					  </div>
					</div>
					</form>
					<div class="row">
					  <div class="col-8">
					  <span id="fill" style="color:red;display:none;">** Please Fill mandatory field</span>
					  </div>
					  <!-- /.col -->
					  <div class="col-4">
						<button type="submit" name="submit" id="add" onclick="add_users()" class="btn btn-primary btn-block">Register</button>
						<button style="display:none;" type="submit" name="submit" id="update" onclick="update_user()" class="btn btn-primary btn-block">Update</button>
						
					  </div>
					  <!-- /.col -->
					</div>
				</div>
   
			</div>
		</div>
        <div class="container-fluid">
		<div class="row">
          <div class="col-12">
			<div class="card">
              <div class="card-header">
                <h3 class="card-title">Users List </h3>

               <!-- <div class="card-tools">
                  <div class="input-group input-group-sm" style="width: 150px;">
                    <input type="text" name="table_search" class="form-control float-right" placeholder="Search">

                    <div class="input-group-append">
                      <button type="submit" class="btn btn-default"><i class="fas fa-search"></i></button>
                    </div>
                  </div>
                </div>
              </div>!-->
              <!-- /.card-header -->
              <div class="card-body table-responsive p-0">
                <table class="table table-hover text-nowrap " >
                  <thead>
                    <tr>
                      <th>S.no</th>
					  <th>Name</th>
                      <th>User Name</th>
                      <th>Mobile</th>
                     
                      <th>Role</th>
					  <th>Action</th>
                    </tr>
                  </thead>
                  <tbody id="table_data">
              
                   
                  </tbody>
                </table>
              </div>
              <!-- /.card-body -->
            </div>
          </div>
          </div>
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->

        
        <!-- Main row -->
       

            <!-- PRODUCT LIST -->
           
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!--/. container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->

 <?php include('footer.php'); ?>
</div>
<!-- ./wrapper -->

<!-- REQUIRED SCRIPTS -->
<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- overlayScrollbars -->
<script src="plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.js"></script>

<!-- OPTIONAL SCRIPTS -->
<script src="dist/js/demo.js"></script>

<!-- PAGE PLUGINS -->
<!-- jQuery Mapael -->
<script src="plugins/jquery-mousewheel/jquery.mousewheel.js"></script>
<script src="plugins/raphael/raphael.min.js"></script>
<script src="plugins/jquery-mapael/jquery.mapael.min.js"></script>
<script src="plugins/jquery-mapael/maps/usa_states.min.js"></script>
<!-- ChartJS -->
<script src="plugins/chart.js/Chart.min.js"></script>
<script src="dist/js/pages/dashboard2.js"></script>
<script>
function mobile_error(){
	if($('#user_mobile').val().length >10 || $('#user_mobile').val().length < 10){
		
		$('#user_mobile').css('border','1px solid red');
		$('#error_mobile').html('** Mobile Number should be 10 charactor');
		$('#error_mobile').css('display','inline');
		$("#add").prop('disabled', true);
		$("#update").prop('disabled', true);
		return false;
	}
	else{
		$("#add").prop('disabled', false);
		$("#update").prop('disabled', false);
		$('#user_mobile').css('border','1px solid #8080805e');
		$('#error_mobile').css('display','none');
		return true;
	}
}
function cpassword1(){

	if($('#password').val()==$('#cpassword').val()){
		
		$('#pass_error').css('display','none');
		$("#add").prop('disabled', false);
	}else{
		
		$('#pass_error').css('display','inline');
		$("#add").prop('disabled', true);
	}
}
function pass_len_update(){
	if($('#update_password').val().length < 6){
		$('#pass_len_update').css('display','inline');
		$("#update").prop('disabled', true);
	}
	else{
		$('#pass_len_update').css('display','none');
		$("#update").prop('disabled', false);
	}
}
function pass_len(){
	if($('#password').val().length < 6){
		$('#pass_len').css('display','inline');
		$("#add").prop('disabled', true);
	}
	else{
		$('#pass_len').css('display','none');
		$("#add").prop('disabled', false);
	}
}
$( document ).ready(function() {	
	view_users();
});
function view_users(){
	
	var dataString = 'type=view_users';
    $.ajax({
		url:'backend/add_update_users.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			
				$('#table_data').html(data);
				
		}
	});
}
function add_users(){	
	if(validation()==true){
	var data=$('form[name=user_form]').serialize();
	var dataString='type=add_user&'+data;
	
	$.ajax({
		url:'backend/add_update_users.php',
		type: 'POST',
        data:dataString,
		success: function(message){
			
			if(message==1){
				$('input').val('');
				$("#mydiv").load(location.href + " #mydiv");
				$('#success').val('Successfully Added');
				$('.alert').css('display','inline');
				$('input').css('border','1px solid #ced4da');
	        	$('select').css('border','1px solid #ced4da');
				view_users();
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
		}
	});
}
}
function edit_user(id){
	//alert(id);
	var dataString = 'id='+id+ '&type=edit_user';
	$.ajax({
		url:'backend/add_update_users.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#add').css('display','none');
				$('#update').css('display','inline');
				console.log(data);
				var obj = JSON.parse(data);
				console.log(obj);
				$('#table_id').val(obj[0].id);
				$('#name').val(obj[0].name);
				$('#user_name').val(obj[0].username);
				$('#user_mobile').val(obj[0].mobile);
				$('.password').css('display','none');
				$('.cpassword').css('display','none');
				//$('.update_password').css('display','inline');
				//$('#update_password').val(obj[0].password);
				$('#i'+obj[0].role).prop("selected", true);	

				
		}
	});
}
function update_user(){
	var data=$('form[name=user_form]').serialize();
	var dataString='type=update_user&'+data+'&id='+$('#table_id').val();
	$.ajax({
		url:'backend/add_update_users.php',
		type: 'POST',
        data:dataString,
		success: function(message){
			
			if(message==1){
				$('#add').css('display','inline');
				$('#update').css('display','none');
				$('.password').css('display','inline-flex');
				$('.cpassword').css('display','inline-flex');
				$('.update_password').css('display','none');
				$('input').val('');
				$('#success').val('Successfully updated record');
				$('.alert').css('display','inline');
				view_users();
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
		}
	});
}
function delete_user(id){
	if(confirm('Are you sure Want to Delete the selected Record')==false){
		return false;
	}
	var dataString = 'id='+id+ '&type=delete_user';
	$.ajax({
		url:'backend/add_update_users.php',
		type: 'POST',
        data:dataString,
		success: function(message){	
				if(message==1){
				$('#role').val('');
				$('#name').val('');
				$('#username').val('');
				$('#password').val('');
				$('#success').val('Successfully deleted');
				$('.alert').css('display','inline');
				view_users();
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
		}
	});
}
function validation(){
	if($('#name').val()=='' || $('#cpassword').val()=='' || $('#username').val()=='' || $('#role').val()==0 || $('#password').val()=='' || $('#user_mobile').val()=='' ){
			$("#add").prop('disabled', true);
			$("#update").prop('disabled', true);
			if($('#name').val()==''){$('#name').css('border','1px solid red')}
			if($('#cpassword').val()==''){$('#cpassword').css('border','1px solid red')}
			if($('#username').val()==''){$('#username').css('border','1px solid red')}
			if($('#user_mobile').val()==''){$('#user_mobile').css('border','1px solid red')}
			if($('#role').val()==0){$('#role').css('border','1px solid red')}
			if($('#password').val()==''){$('#password').css('border','1px solid red')}
			$('#fill').css('display','inline');
			return false;
	}
	else if($('#user_mobile').val().length >10 || $('#user_mobile').val().length < 10){
		$('input').css('border','1px solid #8080805e');
		$('#user_mobile').css('border','1px solid red');
		$('#error_mobile').html('** Mobile Number should be 10 charactor');
		$('#error_mobile').css('display','inline');
		return false;
	}
	else{
		$("#add").prop('disabled', false);
		$("#update").prop('disabled', false);
		$('#error_mobile').css('display','none');
		$('input').css('border','1px solid #ced4da');
		$('select').css('border','1px solid #ced4da');
		$('#fill').css('display','none');
		return true;
	}
}
</script>
</body>
</html>
