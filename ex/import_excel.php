<?php

use Box\Spout\Reader\ReaderFactory;
use Box\Spout\Common\Type;

require_once ('connect.php');
require_once ('Spout/Autoloader/autoload.php');

if(!empty($_FILES['excelfile']['name']))
{
    // Get File extension eg. 'xlsx' to check file is excel sheet
    $pathinfo = pathinfo($_FILES['excelfile']['name']);

    // check file has extension xlsx, xls and also check
    // file is not empty
    if (($pathinfo['extension'] == 'xlsx' || $pathinfo['extension'] == 'xls')
        && $_FILES['excelfile']['size'] > 0 )
    {
        $file = $_FILES['excelfile']['tmp_name'];

        // Read excel file by using ReadFactory object.
        $reader = ReaderFactory::create(Type::XLSX);

        // Open file
        $reader->open($file);
        $count = 0;

        // Number of sheet in excel file
        foreach ($reader->getSheetIterator() as $sheet)
        {

            // Number of Rows in Excel sheet
            foreach ($sheet->getRowIterator() as $row)
            {

                // It reads data after header. In the my excel sheet,
                // header is in the first row.
                if ($count > 0) {

                    // Data of excel sheet
                    $id=$row[0];
                    $name=$row[1];
                    $Salutation = $row[2];
                    $custom_salutation=$row[3];
                    $relation=$row[4];
                    $define_relation=$row[5];
                    $dob=$row[6];
                    $dom=$row[7];
                    $address1=$row[8];
                    $address2=$row[9];
                    $thesil=$row[10];
                    $pincode=$row[11];
                    $post=$row[12];
                    $district=$row[13];
                    $state=$row[14];
                    $contact1=$row[15];
                    $contact2=$row[16];
                    $ppo_number=$row[17];
                    $bank_account=$row[18];
                    $education_year=$row[19];
                    $note=$row[20];
                    $dox=$row[21];
                    $dox_type=$row[22];
                    $dox_page_number=$row[23];
                    $dox_year=$row[24];
                    $army_no=$row[25];

//print_r($row);
                    
                    
                    //Here, You can insert data into database.

                    // $qry = "INSERT INTO `users`(`name`, `mobile_no`) VALUES ('$name','$mobile')";
                    $qry = "UPDATE `sambandh_nok`  SET name='$name', Salutation='$Salutation', custom_salutation='$custom_salutation',
                    relation='$relation', define_relation='$define_relation', dob='$dob',dom='$dom',address1='$address1',address2='$address2',thesil='$thesil',
                    pincode='$pincode',post='$post',district='$district',state='$state',contact1='$contact1',contact2='$contact2',ppo_number='$ppo_number',
                    bank_account='$bank_account',education_year='$education_year',note='$note',dox='$dox',dox_type='$dox_type',dox_page_number='$dox_page_number',
                    dox_year='$dox_year',army_no='$army_no' WHERE id='$id'";
                    
                   // echo $qry;
                   // exit;
                    
                    $res = mysqli_query($con,$qry);

                }
                $count++;
            }
        }



?>
