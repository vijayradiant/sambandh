<?PHP 

    INCLUDE('config.php');
    if($_GET['Criteria']=='Equal'){
	$where="WHERE ".$_GET['Field']."='".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Not_Equal'){
	$where="WHERE ".$_GET['Field']." !=   '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Contains'){
	$where="WHERE ".$_GET['Field']." LIKE  '%".$_GET['Value']."%'";
	}
	elseif($_GET['Criteria']=='Begins_with'){
	$where="WHERE ".$_GET['Field']." LIKE  '".$_GET['Value']."%'";
	}
	elseif($_GET['Criteria']=='Ends_with'){
	$where="WHERE ".$_GET['Field']." LIKE  '%".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Greater_than'){
	$where="WHERE ".$_GET['Field']." >  '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Greater_than_or_Equal_to'){
	$where="WHERE ".$_GET['Field']." >=  '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Less_than'){
	$where="WHERE ".$_GET['Field']." <  '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Less_than_or_Equal_to'){
	$where="WHERE ".$_GET['Field']." <=  '".$_GET['Value']."'";
	}
	////////
	if($_GET['Module']=='Sainik'){
		$ARMY_NO_QUERY="SELECT army_no FROM sambandh_parent ".$where." ";
	}
	elseif($_GET['Module']=='Nok'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_nok`  ".$where." group by army_no";
	}
	elseif($_GET['Module']=='Family'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_family`  ".$where." group by army_no";
	}
	elseif($_GET['Module']=='Unit'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_unit`  ".$where." group by army_no";
	}
	elseif($_GET['Module']=='Others'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_others`  ".$where." group by army_no";
	}
/** Error reporting */
@session_start();
error_reporting(E_ALL & ~E_DEPRECATED & ~E_STRICT & ~E_NOTICE);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
ini_set('memory_limit', '-1');
date_default_timezone_set('Europe/London');

if (PHP_SAPI == 'cli')
	die('This example should only be run from a Web Browser');

/** Include PHPExcel */
require_once('Classes/PHPExcel.php');

$cacheMethod = PHPExcel_CachedObjectStorageFactory:: cache_to_phpTemp;
$cacheSettings = array( 'memoryCacheSize' => '128MB');
PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator("Maarten Balliauw")
							 ->setLastModifiedBy("Maarten Balliauw")
							 ->setTitle("Office 2007 XLSX Test Document")
							 ->setSubject("Office 2007 XLSX Test Document")
							 ->setDescription("Test document for Office 2007 XLSX, generated using PHP classes.")
							 ->setKeywords("office 2007 openxml php")
							 ->setCategory("Test result file");

////////////////////////////////////*SAINIK*///////////////////////////////
$sainik_title =array('Army no','Rank','Name','Date of Cas','Serving Unit','Parent Unit');
$objPHPExcel->setActiveSheetIndex(0);
$x=1;
$sainik_range=range('A','F');
foreach($sainik_title as $key=>$val) {	
    $objPHPExcel->getActiveSheet()->setCellValue($sainik_range[$key].$x, $val);
}
$result = mysqli_query($con,"SELECT * FROM `sambandh_parent`  WHERE army_no IN(".$ARMY_NO_QUERY.")");

$i=2;
if(mysqli_num_rows($result)>0){
	while($row = mysqli_fetch_array($result)) {
		
		$objPHPExcel->getActiveSheet()->setCellValue("A$i",$row['army_no']);
		$objPHPExcel->getActiveSheet()->setCellValue("B$i",$row['rank']);
		$objPHPExcel->getActiveSheet()->setCellValue("C$i",$row['name']);
		$objPHPExcel->getActiveSheet()->setCellValue("D$i",$row['cas_dt']);
		$objPHPExcel->getActiveSheet()->setCellValue("E$i",$row['serving_unit']);
		$objPHPExcel->getActiveSheet()->setCellValue("F$i",$row['parent_unit']);
		
	$i++;
	}
}
$objPHPExcel->getActiveSheet()->setTitle('SAINIK');
/*SAINIK*/


///////////////////////////////////////////////*NOK*//////////////////////////////////////////

$nok_title  =array('Salutation','Name','Relation','Dob','Address1','Address2','Post','Tehsil','District','State','Pincode','Contact 1','Contact 2','PPO number','Bank Name','Bank Account','Vocation','Education qualification','Year of Educational Qualification','Remarks','ARMY NO');

/* Create a new worksheet, after the default sheet*/
$objPHPExcel->createSheet();

/* Add some data to the second sheet, resembling some different data types*/
$objPHPExcel->setActiveSheetIndex(1);
$y=1;
$nok_range=range('A','U');
foreach($nok_title as $key=>$val) {	
    $objPHPExcel->getActiveSheet()->setCellValue($nok_range[$key].$y, $val);
}
// echo "SELECT * FROM `sambandh_nok`  WHERE army_no IN(".$ARMY_NO_QUERY.")";
// exit;
 $result = mysqli_query($con,"SELECT * FROM `sambandh_nok`  WHERE army_no IN(".$ARMY_NO_QUERY.")");
$j=2;
if(mysqli_num_rows($result)>0){
	while($row= mysqli_fetch_array($result)) {
		$objPHPExcel->getActiveSheet()->setCellValue("A$j",$row['Salutation']);
		$objPHPExcel->getActiveSheet()->setCellValue("B$j",$row['name']);
		$objPHPExcel->getActiveSheet()->setCellValue("C$j",$row['relation']);
		$objPHPExcel->getActiveSheet()->setCellValue("D$j",$row['dob']);
		$objPHPExcel->getActiveSheet()->setCellValue("E$j",$row['address1']);
		$objPHPExcel->getActiveSheet()->setCellValue("F$j",$row['address2']);
		$objPHPExcel->getActiveSheet()->setCellValue("G$j",$row['post']);
		$objPHPExcel->getActiveSheet()->setCellValue("H$j",$row['thesil']);
		$objPHPExcel->getActiveSheet()->setCellValue("I$j",$row['district']);
		$objPHPExcel->getActiveSheet()->setCellValue("J$j",$row['state']);
		$objPHPExcel->getActiveSheet()->setCellValue("K$j",$row['pincode']);
		$objPHPExcel->getActiveSheet()->setCellValue("L$j",$row['contact1']);
		$objPHPExcel->getActiveSheet()->setCellValue("M$j",$row['contact2']);
		$objPHPExcel->getActiveSheet()->setCellValue("N$j",$row['ppo_number']);
		$objPHPExcel->getActiveSheet()->setCellValue("O$j",$row['bank_name']);
		$objPHPExcel->getActiveSheet()->setCellValue("P$j",$row['bank_account']);
		$objPHPExcel->getActiveSheet()->setCellValue("Q$j",$row['vocation']);
		$objPHPExcel->getActiveSheet()->setCellValue("R$j",$row['education']);
		$objPHPExcel->getActiveSheet()->setCellValue("S$j",$row['education_year']);
		$objPHPExcel->getActiveSheet()->setCellValue("T$j",'');
		$objPHPExcel->getActiveSheet()->setCellValue("U$j",$row['army_no']);
		
	$j++;
	}
}
$objPHPExcel->getActiveSheet()->setTitle('NOK');
/*NOK*/


///////////////////////////////////////////////*FAMILY*//////////////////////////////////////////

$family_TITLE   =array('Salutation','Name','Relation','Marital Status','Dob','Address1','Address2','Post','Tehsil','District','State','Pincode','Contact Number 1','Contact Number 1','Vocation','Education qualification','Year of Educational Qualification','Remarks','ARMY_NO');

/* Create a new worksheet, after the default sheet*/
$objPHPExcel->createSheet();

/* Add some data to the second sheet, resembling some different data types*/
$objPHPExcel->setActiveSheetIndex(2);
$Z=1;
$nok_range=range('A','S');
foreach($family_TITLE as $key=>$val) {	
    $objPHPExcel->getActiveSheet()->setCellValue($nok_range[$key].$Z, $val);
}

$result = mysqli_query($con,"SELECT * FROM `sambandh_family`  WHERE army_no IN(".$ARMY_NO_QUERY.")");
$j=2;
if(mysqli_num_rows($result)>0){
while($row= mysqli_fetch_array($result)) {
	$objPHPExcel->getActiveSheet()->setCellValue("A$j",$row['Salutation']);
	$objPHPExcel->getActiveSheet()->setCellValue("B$j",$row['name']);
	$objPHPExcel->getActiveSheet()->setCellValue("C$j",$row['relation']);
	$objPHPExcel->getActiveSheet()->setCellValue("D$j",$row['mr_status']);
	$objPHPExcel->getActiveSheet()->setCellValue("E$j",$row['dob']);
	$objPHPExcel->getActiveSheet()->setCellValue("F$j",$row['address1']);
	$objPHPExcel->getActiveSheet()->setCellValue("G$j",$row['address2']);
	$objPHPExcel->getActiveSheet()->setCellValue("H$j",$row['post']);
	$objPHPExcel->getActiveSheet()->setCellValue("I$j",$row['thesil']);
	$objPHPExcel->getActiveSheet()->setCellValue("J$j",$row['district']);
	$objPHPExcel->getActiveSheet()->setCellValue("K$j",$row['state']);
	$objPHPExcel->getActiveSheet()->setCellValue("L$j",$row['pincode']);
	$objPHPExcel->getActiveSheet()->setCellValue("M$j",$row['contact1']);
	$objPHPExcel->getActiveSheet()->setCellValue("N$j",$row['contact2']);
	$objPHPExcel->getActiveSheet()->setCellValue("O$j",$row['vocation']);
	$objPHPExcel->getActiveSheet()->setCellValue("P$j",$row['education']);
	$objPHPExcel->getActiveSheet()->setCellValue("Q$j",$row['education_year']);
	$objPHPExcel->getActiveSheet()->setCellValue("R$j",'');
	$objPHPExcel->getActiveSheet()->setCellValue("S$j",$row['army_no']);
	
$j++;
}
}
$objPHPExcel->getActiveSheet()->setTitle('FAMILY');

/*FAMILY*/


///////////////////////////////////////////////*UNIT*//////////////////////////////////////////

$unit_TITLE  =array('Salutation','Name','Appointment','Unit/Org','Contact No 1','Contact No 2','Email','Remarks','ARMY NO');

/* Create a new worksheet, after the default sheet*/
$objPHPExcel->createSheet();

/* Add some data to the second sheet, resembling some different data types*/
$objPHPExcel->setActiveSheetIndex(3);
$Z=1;
$nok_range=range('A','I');
foreach($unit_TITLE as $key=>$val) {	
    $objPHPExcel->getActiveSheet()->setCellValue($nok_range[$key].$Z, $val);
}

$result = mysqli_query($con,"SELECT * FROM `sambandh_unit`  WHERE army_no IN(".$ARMY_NO_QUERY.")");
$j=2;
if(mysqli_num_rows($result)>0){
while($row= mysqli_fetch_array($result)) {
	$objPHPExcel->getActiveSheet()->setCellValue("A$j",$row['Salutation']);
	$objPHPExcel->getActiveSheet()->setCellValue("B$j",$row['name']);
	$objPHPExcel->getActiveSheet()->setCellValue("C$j",$row['appointment']);
	$objPHPExcel->getActiveSheet()->setCellValue("D$j",$row['unit_org']);
	$objPHPExcel->getActiveSheet()->setCellValue("E$j",$row['contact1']);
	$objPHPExcel->getActiveSheet()->setCellValue("F$j",$row['contact2']);
	$objPHPExcel->getActiveSheet()->setCellValue("G$j",$row['email']);
	$objPHPExcel->getActiveSheet()->setCellValue("H$j",'');
	$objPHPExcel->getActiveSheet()->setCellValue("I$j",$row['army_no']);
	
	
$j++;
}
}
$objPHPExcel->getActiveSheet()->setTitle('UNIT');
/*UNIT*/


///////////////////////////////////////////////*OTHERS*//////////////////////////////////////////

$other_title   =array('Salutation','Name','Appointment','Unit/Org','Contact No 1','Contact No 2','Email','Remarks','ARMY NO');

/* Create a new worksheet, after the default sheet*/
$objPHPExcel->createSheet();

/* Add some data to the second sheet, resembling some different data types*/
$objPHPExcel->setActiveSheetIndex(4);
$Z=1;
$nok_range=range('A','I');
foreach($other_title as $key=>$val) {	
    $objPHPExcel->getActiveSheet()->setCellValue($nok_range[$key].$Z, $val);
}

$result = mysqli_query($con,"SELECT * FROM `sambandh_others`  WHERE army_no IN(".$ARMY_NO_QUERY.")");
$j=2;
if(mysqli_num_rows($result)>0){
while($row= mysqli_fetch_array($result)) {
		$objPHPExcel->getActiveSheet()->setCellValue("A$j",$row['Salutation']);
	$objPHPExcel->getActiveSheet()->setCellValue("B$j",$row['name']);
	$objPHPExcel->getActiveSheet()->setCellValue("C$j",$row['appointment']);
	$objPHPExcel->getActiveSheet()->setCellValue("D$j",$row['unit_org']);
	$objPHPExcel->getActiveSheet()->setCellValue("E$j",$row['contact1']);
	$objPHPExcel->getActiveSheet()->setCellValue("F$j",$row['contact2']);
	$objPHPExcel->getActiveSheet()->setCellValue("G$j",$row['email']);
	$objPHPExcel->getActiveSheet()->setCellValue("H$j",'');
	$objPHPExcel->getActiveSheet()->setCellValue("I$j",$row['army_no']);

	
	
$j++;
}
}
$objPHPExcel->getActiveSheet()->setTitle('OTHERS');

//////////////////////////////////////////////////////////////////////////////
date_default_timezone_set("Asia/Kolkata");
$date=date("d-m-Y h:i:s");
  $filename='Query_"'.$date.'".xlsx';

// Redirect output to a client’s web browser (Excel2007)
header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
header('Content-Disposition: attachment;filename='.$filename.'');
header('Cache-Control: max-age=0');
  header('Content-Type: charset=UTF-8');
// If you're serving to IE 9, then the following may be needed
header('Cache-Control: max-age=1');

// If you're serving to IE over SSL, then the following may be needed
header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
header ('Pragma: public'); // HTTP/1.0

$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
$objWriter->save('php://output');
?>
