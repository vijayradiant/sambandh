<?php
INCLUDE('config.php');
$csv = "ARMY NO,RANK,SAINIK NAME,DATE OF CAS,SERVING UNIT,PARENT UNIT,SALUTATION,NOK NAME,NOK RELATIOSHIP,SAATHI NAME,	SAATHI TELEPHONE NUMBER,ERRORS \n";//Column headers
if($_POST["type"]=='bulk'){ 
          $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		  $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $rank = $filesop[1];
				   $SAINIK_NAME = $filesop[2];
				   $date = $filesop[3];
				   $SERVING_UNIT = $filesop[4];
				   $PARENT_UNIT = $filesop[5];
				   $SALUTATION = $filesop[6];
				   $NOK_NAME = $filesop[7];
				   $NOK_RELATIOSHIP = $filesop[8];
				   $SAATHI_NAME = $filesop[9];
                   $SAATHI_TELEPHONE_NUMBER = $filesop[10];
				   $date = strtotime($date); 
				   $cas_dt= date('Y-m-d', $date); 
				   

				   if($c!=0){
				   if($armyno!=''){
					   
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'Duplicate Army number'));
					   } 
					   elseif($armyno=='' || $SAINIK_NAME=='' ){
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no-sainik name-cas date are mandatory fields")); 
					   }
					   else{
					    $sql = "insert into sambandh_parent(army_no,name,cas_dt,rank,serving_unit,parent_unit) values ('$armyno','$SAINIK_NAME','$cas_dt','$rank','$SERVING_UNIT','$PARENT_UNIT')";
					    $stmt = mysqli_query($con,$sql);
						
						$qry1q="select army_no FROM sambandh_nok WHERE army_no='".$armyno."'";
					    $stmt1q = mysqli_query($con,$qry1q);
						if(mysqli_num_rows($stmt1q)>0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'NOK Exits for this Army Number'));
					    }else{
							$sql1 = "insert into sambandh_nok(name,Salutation,relation,army_no) values ('$NOK_NAME','$SALUTATION','$NOK_RELATIOSHIP','$armyno')";
					        $stmt1 = mysqli_query($con,$sql1);
						}
						$sql2 = "insert into sambandh_unit(unit_org,army_no) values ('$SERVING_UNIT','$armyno')";
						$stmt2 = mysqli_query($con,$sql2);
						$sql3 = "insert into sambandh_others(name,contact1,army_no) values ('$SAATHI_NAME','$SAATHI_TELEPHONE_NUMBER','$armyno')";
						$stmt3 = mysqli_query($con,$sql3);
					   }
				   }
				   }
				   $c = $c + 1;
            }
				
            	if(count($items) == 0){
					echo 1;
				}
				else{
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}
			  

 }   
elseif($_POST["type"]=='sainik'){     
          $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		  $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $rank = $filesop[1];
				   $SAINIK_NAME = $filesop[2];
				   $date = $filesop[3];
				   $SERVING_UNIT = $filesop[4];
				   $PARENT_UNIT = $filesop[5];
				   $date = strtotime($date); 
				   $cas_dt= date('d/m/Y', $date); 
				   

				   if($c!=0){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'Duplicate Army number'));
					   } 
					   elseif($armyno=='' || $SAINIK_NAME=='' || $date==''){
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no-sainik name-cas date are mandatory fields")); 
					   }
					   else{
					   $sql = "insert into sambandh_parent(army_no,name,cas_dt,rank,serving_unit,parent_unit) values ('$armyno','$SAINIK_NAME','$cas_dt','$rank','$SERVING_UNIT','$PARENT_UNIT')";
					   $stmt = mysqli_query($con,$sql);
					   }
				   }
				   $c = $c + 1;
            }
				
            	if(count($items) == 0){
					echo 1;
				}
				else{
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}
			  
				  
			

}
ELSEIF($_POST["type"]=='Nok'){
		  $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $SALUTATION = $filesop[6];
				   $NOK_NAME = $filesop[7];
				   $NOK_RELATIOSHIP = $filesop[8];
				   if($c!=0){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   $qry1="select army_no FROM sambandh_nok WHERE army_no='".$armyno."'";
					   $stmt1 = mysqli_query($con,$qry1);
					   if(mysqli_num_rows($stmt)==0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'This Army number not present in sainik'));
					   } 

					   elseif(mysqli_num_rows($stmt1)>0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'NOK Exits for this Army Number'));
					   } 
					   elseif($armyno==''){
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no is mandatory field")); 
					   }
					   else{
						    $sql = "insert into sambandh_nok(name,Salutation,relation,army_no) values ('$NOK_NAME','$SALUTATION','$NOK_RELATIOSHIP','$armyno')";
					        $stmt = mysqli_query($con,$sql);
					   }
					  
				   }
				   $c = $c + 1;
            }
			if(count($items) == 0){
					echo 1;
				}
				else{
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}

           
}
ELSEIF($_POST["type"]=='Unit'){
		  $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $SERVING_UNIT = $filesop[4];
				   if($c!=0){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)==0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'This Army number not present in sainik'));
					   } 
					   elseif($armyno==''){
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no is mandatory field")); 
					   }
					   else{
						   $sql = "insert into sambandh_unit(unit_org,army_no) values ('$SERVING_UNIT','$armyno')";
						   $stmt = mysqli_query($con,$sql);
					   }
				   }
				   $c = $c + 1;
            }

            if(count($items) == 0){
					echo 1;
			}
			else{
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}
}
ELSEIF($_POST["type"]=='Others'){
		  $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $SAATHI_NAME = $filesop[9];
                   $SAATHI_TELEPHONE_NUMBER = $filesop[10];
				   if($c!=0){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)==0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'This Army number not present in sainik'));
					   } 
					   elseif($armyno==''){
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no is mandatory field")); 
					   }
					    else{
						   $sql = "insert into sambandh_others(name,contact1,army_no) values ('$SAATHI_NAME','$SAATHI_TELEPHONE_NUMBER','$armyno')";
						   $stmt = mysqli_query($con,$sql);
					   }
				   }
				   $c = $c + 1;
            }

            if(count($items) == 0){
					echo 1;
			}
			else{
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
			}
}
ELSEIF($_POST["type"]=='Family'){
	echo 'Family';
}


?>