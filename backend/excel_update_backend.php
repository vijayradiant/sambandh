<?php
INCLUDE('config.php');

if($_POST["type"]=='bulk'){ 
          $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		  $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $rank = $filesop[1];
				   $SAINIK_NAME = $filesop[2];
				   $date = $filesop[3];
				   $SERVING_UNIT = $filesop[4];
				   $PARENT_UNIT = $filesop[5];
				   $SALUTATION = $filesop[6];
				   $NOK_NAME = $filesop[7];
				   $NOK_RELATIOSHIP = $filesop[8];
				   $SAATHI_NAME = $filesop[9];
                   $SAATHI_TELEPHONE_NUMBER = $filesop[10];
				   $date = strtotime($date); 
				   $cas_dt= date('Y-m-d', $date); 
				   

				   if($c!=0){
				   if($armyno!=''){
					   
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'Duplicate Army number'));
					   } 
					   elseif($armyno=='' || $SAINIK_NAME=='' ){
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no-sainik name-cas date are mandatory fields")); 
					   }
					   else{
					    $sql = "insert into sambandh_parent(army_no,name,cas_dt,rank,serving_unit,parent_unit) values ('$armyno','$SAINIK_NAME','$cas_dt','$rank','$SERVING_UNIT','$PARENT_UNIT')";
					    $stmt = mysqli_query($con,$sql);
						
						$qry1q="select army_no FROM sambandh_nok WHERE army_no='".$armyno."'";
					    $stmt1q = mysqli_query($con,$qry1q);
						if(mysqli_num_rows($stmt1q)>0){
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],'NOK Exits for this Army Number'));
					    }else{
							$sql1 = "insert into sambandh_nok(name,Salutation,relation,army_no) values ('$NOK_NAME','$SALUTATION','$NOK_RELATIOSHIP','$armyno')";
					        $stmt1 = mysqli_query($con,$sql1);
						}
						$sql2 = "insert into sambandh_unit(unit_org,army_no) values ('$SERVING_UNIT','$armyno')";
						$stmt2 = mysqli_query($con,$sql2);
						$sql3 = "insert into sambandh_others(name,contact1,army_no) values ('$SAATHI_NAME','$SAATHI_TELEPHONE_NUMBER','$armyno')";
						$stmt3 = mysqli_query($con,$sql3);
					   }
				   }
				   }
				   $c = $c + 1;
            }
				
            	if(count($items) == 0){
					echo 1;
				}
				else{
					$csv = "ARMY NO,RANK,SAINIK NAME,DATE OF CAS,SERVING UNIT,PARENT UNIT,SALUTATION,NOK NAME,NOK RELATIOSHIP,SAATHI NAME,	SAATHI TELEPHONE NUMBER,ERRORS \n";//Column headers
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}
			  

 }   
elseif($_POST["type"]=='sainik'){     
          $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		  $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $armyno = $filesop[0];
				   $rank = $filesop[1];
				   $SAINIK_NAME = $filesop[2];
				   $date = $filesop[3];
				   $SERVING_UNIT = $filesop[4];
				   $PARENT_UNIT = $filesop[5];
				   $date = strtotime($date); 
				   $cas_dt= date('d/m/Y', $date); 
				   

				   if($c!=0){
					   if($armyno==''){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
							if($armyno=='' || $SAINIK_NAME=='' ){
							    array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],"Army no-sainik name-cas date are mandatory fields")); 
						   }
						   else{
								$sql = "UPDATE  sambandh_parent SET army_no='$armyno',name='$SAINIK_NAME',cas_dt='$cas_dt',rank='$rank',serving_unit='$SERVING_UNIT',parent_unit='$PARENT_UNIT' WHERE army_no='".$armyno."' ";
								$stmt = mysqli_query($con,$sql);	
						   }
						  
					   }
						else{
							array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],' Army number not exits'));
						}
					   
				   }
				   }
				   $c = $c + 1;
            }
				
            	if(count($items) == 0){
					echo 1;
				}
				else{
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}
			  
				  
			

}
ELSEIF($_POST["type"]=='Nok'){
		  $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				    
					$Salutation= $filesop[0];
					$name = $filesop[1];
					$gender= $filesop[2];
					$relation= $filesop[3];
					//$define_relation= $filesop[4];
					$dob= $filesop[5];
					$dom= $filesop[4];
					$address1= $filesop[6];
					$address2= $filesop[7];
					$thesil= $filesop[9];
					$pincode= $filesop[12];
					$post= $filesop[8];
					$district= $filesop[10];
					$state= $filesop[11];
					$contact1= $filesop[13];
					$contact2= $filesop[14];
					$ppo_number= $filesop[15];
					$bank_name= $filesop[16];
					$bank_account= $filesop[17];
					$education= $filesop[19];
					$education_year= $filesop[20];
					$vocation= $filesop[18];
					$note= $filesop[21];
					 $armyno= $filesop[22];
					
					if($c!=0){
					if($armyno!=''){
					    $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   $qry1="select army_no FROM sambandh_nok WHERE army_no='".$armyno."'";
					   $stmt1 = mysqli_query($con,$qry1);
					   if(mysqli_num_rows($stmt)>0 && mysqli_num_rows($stmt1)>0){
						  $sql = "UPDATE sambandh_nok SET name='$name',gender='$gender',Salutation='$Salutation',relation='$relation', 
						  dob='$dob',dom='$dom',address1='$address1',address2='$address2',thesil='$thesil',pincode='$pincode',post='$post',district='$district',state='$state',contact1='$contact1',contact2='$contact2',ppo_number='$ppo_number',bank_name='$bank_name',bank_account='$bank_account',education='$education',education_year='$education_year',vocation='$vocation',note='$note' WHERE army_no='$armyno'"  ;
					      $stmt = mysqli_query($con,$sql);
						
					   } 

					   // elseif($armyno==''){
						  // array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],$filesop[11],$filesop[12],$filesop[13],$filesop[14],$filesop[15],$filesop[16],$filesop[17],$filesop[18],$filesop[19],$filesop[20],$filesop[21],$filesop[22],"Army no is mandatory field")); 
					   // }
					   else{
						  array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],$filesop[11],$filesop[12],$filesop[13],$filesop[14],$filesop[15],$filesop[16],$filesop[17],$filesop[18],$filesop[19],$filesop[20],$filesop[21],$filesop[22],' Army number not exits'));  
					   }
					  
				   }
				   }
				   $c = $c + 1;
            }
			if(count($items) == 0){
					echo 1;
				}
				else{
					$csv="Salutation,Name,Gender,Relation,Date of married,Dob,Address1,Address2,Post,Tehsil,District,State,Pincode,Contact 1,Contact 2,PPO number,Bank Name,Bank Account,Vocation,Education qualification,Year of Educational Qualification,Remarks,ARMY NO,ERRORS \n";
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11].','.$record[12].','.$record[13].','.$record[14].','.$record[15].','.$record[16].','.$record[17].','.$record[18].','.$record[19].','.$record[20].','.$record[21].','.$record[22].','.$record[23]."\n"; //Append data to csv
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}

           
}
ELSEIF($_POST["type"]=='Unit'){
		  $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				    $Salutation= $filesop[0];
					$name= $filesop[1];
					$appointment= $filesop[2];
					$unit_org= $filesop[3];
					$contact1= $filesop[4];
					$contact2= $filesop[5];
					$email= $filesop[6];
					$remarks= $filesop[7];
					$armyno= $filesop[8];
				   if($c!=0){
					   if($armyno!=''){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
						   $sql = "UPDATE sambandh_unit SET    Salutation='$Salutation',name='$name',appointment='$appointment',unit_org='$unit_org',contact1='$contact1',contact2='$contact2',email='$email',remarks='$remarks',army_no='$armyno'"  ;
						   $stmt = mysqli_query($con,$sql);
							
					   } 
					   // elseif($armyno==''){
						  // array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],"Army no is mandatory field")); 
					   // }
					   else{
						 array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],'This Army number not present')); 
					   }
				   }
				   }
				   $c = $c + 1;
            }

            if(count($items) == 0){
					echo 1;
			}
			else{
					$csv="Salutation,Name,Appointment,Unit/Org,ContactNo 1,ContactNo 2,Email,Remarks,ARMY NO,ERROR";
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9]."\n"; //Append data to csv
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
				}
}
ELSEIF($_POST["type"]=='Others'){
		  $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $Salutation= $filesop[0];
					$name= $filesop[1];
					$appointment= $filesop[2];
					$unit_org= $filesop[3];
					$contact1= $filesop[4];
					$contact2= $filesop[5];
					$email= $filesop[6];
					$remarks= $filesop[7];
					$armyno= $filesop[8];
				   if($c!=0){
					   if($armyno!=''){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
							 $sql = "UPDATE sambandh_others SET    Salutation='$Salutation',name='$name',appointment='$appointment',unit_org='$unit_org',contact1='$contact1',contact2='$contact2',email='$email',remarks='$remarks',army_no='$armyno'"  ;
 
						     $stmt = mysqli_query($con,$sql);
					   } 
					   // elseif($armyno==''){
						  // array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],"Army no is mandatory field")); 
					   // }
					    else{  
						   array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],'This Army number not present'));
					   }
				   }
				   }
				   $c = $c + 1;
            }

            if(count($items) == 0){
					echo 1;
			}
			else{
				    $csv="Salutation,Name,Appointment,Unit/Org,ContactNo 1,ContactNo 2,Email,Remarks,ARMY NO,ERROR";
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11]."\n"; //Append data to csv
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
			}
}
ELSEIF($_POST["type"]=='Family'){
	 $file = $_FILES['files']['tmp_name'];
          $handle = fopen($file, "r");
          $c = 0;
		   $items=array();
            while(($filesop = fgetcsv($handle, 1000, ",")) !== false){
				   $Salutation= $filesop[0];
					$name = $filesop[1];
					$gender= $filesop[2];
					$relation= $filesop[3];
					$mr_status= $filesop[4];
					$dob= $filesop[6];
					$dom= $filesop[5];
					$address1= $filesop[7];
					$address2= $filesop[8];
					$thesil= $filesop[10];
					$pincode= $filesop[13];
					$post= $filesop[9];
					$district= $filesop[11];
					$state= $filesop[12];
					$contact1= $filesop[14];
					$contact2= $filesop[15];
					$education= $filesop[17];
					$education_year= $filesop[18];
					$vocation= $filesop[16];
					$armyno= $filesop[19];
				   if($c!=0){
					   if($armyno!=''){
					   $qry="select army_no FROM sambandh_parent WHERE army_no='".$armyno."'";
					   $stmt = mysqli_query($con,$qry);
					   if(mysqli_num_rows($stmt)>0){
							$sql = "UPDATE sambandh_family SET name='$name',Salutation='$Salutation',gender='$gender',relation='$relation',mr_status='$mr_status',
							dob='$dob',dom='$dom',address1='$address1',address2='$address2',thesil='$thesil',pincode='$pincode',post='$post',district='$district',state='$state',contact1='$contact1',contact2='$contact2',education='$education',education_year='$education_year',vocation='$vocation' WHERE army_no='$armyno'"  ;
 
 
						     $stmt = mysqli_query($con,$sql);
					   } 
					   // elseif($armyno==''){
						  // array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],"Army no is mandatory field")); 
					   // }
					    else{  
						   array_push($items,array($filesop[0],$filesop[1],$filesop[2],$filesop[3],$filesop[4],$filesop[5],$filesop[6],$filesop[7],$filesop[8],$filesop[9],$filesop[10],$filesop[11],$filesop[12],$filesop[13],$filesop[14],$filesop[15],$filesop[16],$filesop[17],$filesop[18],$filesop[19],' Army number not exits'));  
					   }
				   }
				   }
				   $c = $c + 1;
            }

            if(count($items) == 0){
					echo 1;
			}
			else{
				    $csv="Salutation,Name,Gender,Relation,Marital Status,Date of married,Dob,Address1,Address2,Post,Tehsil,District,State,Pincode,Contact Number 1,Contact Number 1,Vocation,Education qualification,Year of Educational Qualification,ARMY_NO,ERROR";
					foreach ($items as $record){
						$csv.= $record[0].','.$record[1].','.$record[2].','.$record[3].','.$record[4].','.$record[5].','.$record[6].','.$record[7].','.$record[8].','.$record[9].','.$record[10].','.$record[11].','.$record[12].','.$record[13].','.$record[14].','.$record[15].','.$record[16].','.$record[17].','.$record[18].','.$record[19].','.$record[20]."\n";
					}
					$nme="sampledata".date("j-m-Y_H.i"). ".csv";
					$csv_handler = fopen ("excel_error_document/$nme","w");
					fwrite ($csv_handler,$csv);
					fclose ($csv_handler);

					echo $nme;
			}
}


?>