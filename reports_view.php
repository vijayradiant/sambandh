<?php 
session_start();
if(!isset($_SESSION['name'])){
		header('location:login.php');
}
?>
<!DOCTYPE html>
<html>
<?php INCLUDE('head.php');?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>
<style>
#datas{
	    height: 500px;
    overflow: overlay;
}
table {
  font-family: arial, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

td, th {
  border: 1px solid #dddddd;
  text-align: left;
  padding: 3px;
  font-size:12px;
}

tr:nth-child(even) {
  background-color: #dddddd;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>REPORTS</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Reports</li>
            </ol>
          </div>
		   <div class="alert success" style="display:none;">
			  <span class="closebtn">&times;</span>  
			  <strong>Success!</strong> <span id="success"></span>
			</div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Filter</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             
                <div class="card-body">
				  <form action="" name="filter_form">
				  <div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Module</label>
							<select name="Module"class="form-control Module">
								<option value="0">Select Module</option>
								<option value="Sainik">Sainik</option>
								<option value="Nok">Nok</option>
								<option value="Family">Family</option>
								<option value="Unit">Unit</option>
								<option value="Others">Others</option>
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Field</label>
							<select name="Field"class="form-control Field">
								
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Criteria</label>
							<select name="Criteria"class="form-control">
								<option selected value="Equal">Equal </option>
								<option  value="Not_Equal">Not Equal </option>
								<option  value="Contains">Contains </option>
								<option  value="Begins_with">Begins with </option>
								<option  value="Ends_with">Ends with </option>
								<option  value="Greater_than">Greater than </option>
								<option  value="Greater_than_or_Equal_to">Greater than or Equal to </option>
								<option  value="Less_than">Less than </option>
								<option  value="Less_than_or_Equal to">Less than or Equal to </option>
								
							</select>
						</div>
					</div>
					
                    <div class="col-md-3">
						<div class="form-group">
							<label for="">Value</label>
							<input id="value" name="Value" type="text" class="form-control" >
							
						</div>
					</div>
					</form>
				</div>
				<div class="row">
					<div class="col-md-1">
						 <button type="submit" id="view" name="view" onclick="view()" class="btn btn-primary">View</button>
					</div>
					<div class="col-md-2">
						 <button type="submit" id="export" name="export" onclick="export_to_excel()" class="btn btn-primary">Export to excel</button>
					</div>
					<div class="col-md-2">
						 <button type="submit" id="export" name="export" onclick="export_to_pdf()" class="btn btn-primary">Export to pdf</button>
					</div>
				</div>
				  
				  
				 
				  
                </div>
                <!-- /.card-body -->

              
            
            </div>
            <!-- /.card -->

          
              
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
	  <div id="datas">
	  </div>
    </section>
    <!-- /.content -->
  </div>
<?php include('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="plugins/select2/js/select2.full.min.js"></script>

<script src="dist/js/adminlte.min.js"></script>

<script src="dist/js/demo.js"></script>

<script>
function view(){
	var data=$('form[name=filter_form]').serialize();
	var dataString = '&type=view_report&'+data;
	$.ajax({
		url:'backend/reports.php',
		type: 'POST',
        data:dataString,
		success: function(message){
			
			$('#datas').html(message); 
		}
	});
}
function export_to_excel(){
	var data=$('form[name=filter_form]').serialize();
	window.location.href='backend/export_to_excel.php?'+data
}
function export_to_pdf(){
		var data=$('form[name=filter_form]').serialize();
	window.location.href='pdf/test.php?'+data
}
</script>
<script>
var sainik = [
{name: 'Army no',db:'army_no'},
{name: 'Rank',db:'rank'},
{name: 'Name',db:'name'},
{name: 'Serving Unit',db:'serving_unit'},
{name: 'Parent Unit',db:'parent_unit'}
];
var nok = [
{name: 'Salutation',db:'Salutation'},
{name: 'Name',db:'name'},
{name: 'Relation',db:'relation'},
{name: 'Dob',db:'dob'},
{name: 'Address1',db:'address1'},
{name: 'Address2',db:'address2'},
{name: 'Post',db:'post'},
{name: 'Tehsil',db:'thesil'},
{name: 'District',db:'district'},
{name: 'State',db:'state'},
{name: 'Pincode',db:'pincode'},
{name: 'Contact 1',db:'contact1'},
{name: 'Contact 2',db:'contact2'},
{name: 'PPO number',db:'ppo_number'},
{name: 'Bank Name',db:'bank_name'},
{name: 'Bank Account',db:'bank_account'},
{name: 'Vocation',db:'vocation'},
{name: 'Education qualification',db:'education'},
{name: 'Year of Educational Qualification',db:'education_year'}
];
var family = [
{name: 'Salutation',db:'Salutation'},
{name: 'Name',db:'name'},
{name: 'Relation',db:'relation'},
{name: 'Marital Status',db:'mr_status'},
{name: 'Dob',db:'dob'},
{name: 'Address 1',db:'address1'},
{name: 'Address 2',db:'address2'},
{name: 'Post',db:'post'},
{name: 'Tehsil',db:'thesil'},
{name: 'District',db:'district'},
{name: 'State',db:'state'},
{name: 'Pincode',db:'pincode'},
{name: 'Contact Number 1',db:'contact1'},
{name: 'Contact Number 1',db:'contact2'},
{name: 'Vocation',db:'vocation'},
{name: 'Education qualification',db:'education'},
{name: 'Year of Educational Qualification',db:'education_year'}
];
var unit = [
{name: 'Salutation',db:'Salutation'},
{name: 'Name',db:'name'},
{name: 'Appointment',db:'appointment'},
{name: 'Unit/Org',db:'unit_org'},
{name: 'Contact No 1',db:'contact1'},
{name: 'Contact No 2',db:'contact2'},
{name: 'Email',db:'unit_org'}
];
var other = [
{name: 'Salutation',db:'Salutation'},
{name: 'Name',db:'name'},
{name: 'Appointment',db:'appointment'},
{name: 'Unit/Org',db:'unit_org'},
{name: 'Contact No 1',db:'contact1'},
{name: 'Contact No 2',db:'contact2'},
{name: 'Email',db:'unit_org'}
];

$(".Module").change(function(){
    var module = $(this).val();
	
    var options =  '<option value="0"><strong>Fields</strong></option>';
	if(module=='Sainik'){
		 $(sainik).each(function(index, value){ 
            options += '<option value="'+value.db+'">'+value.name+'</option>'; 
		});
		$('.Field').html(options); 
	}
	if(module=='Nok'){
		 $(nok).each(function(index, value){ 
            options += '<option value="'+value.db+'">'+value.name+'</option>'; 
		});
		$('.Field').html(options); 
	}
	if(module=='Family'){
		 $(family).each(function(index, value){ 
            options += '<option value="'+value.db+'">'+value.name+'</option>'; 
		});
		$('.Field').html(options); 
	}
	if(module=='Unit'){
		 $(unit).each(function(index, value){ 
            options += '<option value="'+value.db+'">'+value.name+'</option>'; 
		});
		$('.Field').html(options); 
	}
	if(module=='Others'){
		 $(others).each(function(index, value){ 
            options += '<option value="'+value.db+'">'+value.name+'</option>'; 
		});
		$('.Field').html(options); 
	}
   
});
</script>
</body>
</html>
