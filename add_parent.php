<?php 
session_start();
if(!isset($_SESSION['name'])){
		header('location:login.php');
}
?>
<!DOCTYPE html>
<html>
<?php INCLUDE('head.php');?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>
<style>
.autocomplete-items {
 position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    max-height: 300px;
    overflow-y: auto;
    width: 93%;
    box-shadow: 0px 2px 2px 1px #8b45130d;
}

.autocomplete-items div {
    padding: 5px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
}
.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 37px;
    user-select: none;
    -webkit-user-select: none;
	border: 1px solid #8080805e;
}
.dateselector > .selector-controls-container {
    width: 128px;
    display: contents;
    padding: 10px 10px 10px 10px;
}
.dateselector{
	width:90%;
}
.the-datepicker__deselect-button {
    text-decoration: none;
    color: #007eff;
    font-weight: bold;
    display: none;
}
</style>


  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Add new Sainik</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add new Sainik</li>
            </ol>
          </div>
		   <div class="alert success" style="display:none;">
			  <span class="closebtn">&times;</span>  
			  <strong>Success!</strong> <span id="success"></span>
			</div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Sainik</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             
                <div class="card-body">
				  <form action="" name="parent_form">
				  <div class="row">
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Pers no</label>
							 <input type="hidden" name="table_id" id="table_id">
							<input type="number" id="army_no" name="army_no" class="form-control" id="" placeholder="Enter Army no">
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Rank</label>
							<input type="text" id="rank" name="rank" class="form-control" style="width: 100%;">
							
							</select>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Name</label>
							<input type="text" id="name" name="name" class="form-control" id="" placeholder="Enter Name">
						</div>
					</div>
					<!--<div class="col-md-3">
						<div class="form-group">
							<label for="">Salutation</label>
							<select id="Salutation" name="Salutation" class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
								                    
							</select>
						</div>
					</div>!-->
                    <div class="col-md-3">
						<div class="form-group">
							<label for="">Date of Cas</label>
							<input id="cas_dt" name="cas_dt" type="text" class="form-control" placeholder="DD.MM.YYYY">
							<ul class="settings" style="display:none;">
				<li>
					<label><span id="selectedDate"></span>;</label>
				</li>
				<li>
					<label> <span id="selectedDateFormatted"></span>;</label>
				</li>
				<li>
					<label><span id="currentMonth"></span>;</label>
				</li>
            </ul>
						</div>
					</div>
					<div class="col-md-3">
						<div class="form-group">
							<label for="">Serving Unit</label>
							<input type="text" id="serving_unit" name="serving_unit" class="form-control"  placeholder="Enter Name">
						</div>
				  </div>
				  <div class="col-md-3">
						<div class="form-group">
							<label for="">Parent Unit</label>
							<input type="text" id="parent_unit" name="parent_unit" class="form-control"  placeholder="Enter Name">
						</div>
					</div>
                  </div>
				  
				   </form>
				 
				  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" id="add" name="add" onclick="add_members()" class="btn btn-primary">Submit</button>
				  <span style="color:red;display:none;" id="error"></span>
				 
                </div>
            
            </div>
            <!-- /.card -->

          
              
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php include('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="plugins/select2/js/select2.full.min.js"></script>

<script src="dist/js/adminlte.min.js"></script>

<script src="dist/js/demo.js"></script>
<script src="sainik_rank.js"></script>


<!-----DATE PICKER----->
<link rel="stylesheet" href="dist/the-datepicker.css">
<script src="dist/the-datepicker.js"></script>
<script>
		(function () {
			var input = document.getElementById('cas_dt');

			var datepicker = new TheDatepicker.Datepicker(input);

			var selectedDateOutput = document.getElementById('selectedDate');
			var selectedDateFormattedOutput = document.getElementById('selectedDateFormatted');
			var updateSelectedDate = function () {
				var selectedDate = datepicker.getSelectedDate();
				selectedDateOutput.innerText = selectedDate !== null ? '.toString() === "' + selectedDate.toString() + '"' : ' === null';
				var selectedDateFormatted = datepicker.getSelectedDateFormatted();
				selectedDateFormattedOutput.innerText = selectedDateFormatted !== null ? '"' + selectedDateFormatted + '"' : 'null';
			};
			datepicker.options.onSelect(updateSelectedDate);
			updateSelectedDate();

			var currentMonthOutput = document.getElementById('currentMonth');
			var updateCurrentMonth = function () {
				var currentMonth = datepicker.getCurrentMonth();
				currentMonthOutput.innerText = currentMonth !== null ? '.toString() === "' + currentMonth.toString() + '"' : ' === null';
			};
			datepicker.options.onMonthChange(updateCurrentMonth);
			updateCurrentMonth();

			datepicker.render();

			
			
		})();
	</script>
<script>


function add_members(){	
	
	if(validation()==true){
	var data=$('form[name=parent_form]').serialize();
	var dataString = '&type=add_members&'+data;
	
	
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(message){
			
			if(message==1){
				
				$('input[type=text]').val('');
				$('input[type=number]').val('');
				$('input[type=email]').val('');
				$('#success').val('Successfully Added');
				$('.alert').css('display','inline');
				$('#error').css('display','none');
				$('#army_no').css('border','1px solid #ced4da');
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
			else if(message==2){
				$('#error').html('** Pers Number Already exits');
				$('#error').css('display','inline');
				$('#army_no').css('border','1px solid red');
			}
		}
	});
}
}


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
function validation(){
	
	
	if($('#army_no').val()=='' || $('#name').val()=='' || $('#cas_dt').val() ==''  || $('#rank').val()==''  ){
		if($('#army_no').val()==''){$('#army_no').css('border','1px solid red')}
		if($('#name').val()==''){$('#name').css('border','1px solid red')}
		if($('#cas_dt').val()==''){$('#cas_dt').css('border','1px solid red')}
		if($('#rank').val()==''){$('#rank').css('border','1px solid red')}
		$('#error').html('** Please Fill mandatory  field');
		$('#error').css('display','inline');
		return false;
	}
	
	else{
		$('#error').css('display','none');
		$('input').css('border','1px solid #8080805e');
		return true;
	}
}
</script>


<!-- my date-selector script -->
<script src="./src/gm-date-selector.js"></script>

<!-- my date-selector config -->
<script type="text/javascript">
    dateSelector( '.dateInput');
</script>
</body>
</html>
