<?php 
session_start();
if(!isset($_SESSION['name'])){
		header('location:login.php');
}
	$action_ar = explode(',', $_SESSION['action']);	
?>
<!DOCTYPE html>
<html>
<?php INCLUDE('head.php');?>
<style>
.autocomplete-items {
 position: absolute;
    border: 1px solid #d4d4d4;
    border-bottom: none;
    border-top: none;
    z-index: 99;
    max-height: 300px;
    overflow-y: auto;
    width: 60%;
    box-shadow: 0px 2px 2px 1px #8b45130d;
}

.autocomplete-items div {
    padding: 5px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
}
.new_dr{
	    left: inherit;
    right: -308px;
    padding: 10px;
    min-width: 456px !important;
    max-width: 456px !important;
}
.add{
	display:inline-flex;
}
.add2{
	margin-left: 10px;
}
.search-border{
        border-bottom: 1px solid #00000033;
        padding:8px;
        border:100%;
    }
.search-border input{
	border: none;
	width:100%;
	font-size:14px;
}
.search-border1 input{
	border: none; 
	border-bottom: 1px solid #00000033;
    padding:8px;
    border:100%;
	font-size:14px;
}
.search-border input:focus{
	
	 outline: none !important;
}
/**#example1_wrapper .row:nth-child(2) {
    overflow-y: scroll;
    max-height: 400px;
}
.sorting_asc:nth-child(1){
	color:white;
	width:10px;
}
.child{
	display:none;
}
table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child:before, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child:before{
	display:none;
	
	
}

table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>td:first-child, table.dataTable.dtr-inline.collapsed>tbody>tr[role="row"]>th:first-child{
	padding-left:16px;
}
#example1_wrapper .col-md-6{
	
}**/
#srch_dr{
	display:none;
}
.dropdown-content {
  display: none;
  position: absolute;
  background-color: #f9f9f9;
  min-width: 160px;
  box-shadow: 0px 8px 16px 0px rgba(0,0,0,0.2);
  z-index: 1;
}
#dt-cell-sellection_length{
	display:none;
}

@media screen and (max-width: 480px) {
  .new_dr {
    left: inherit;
    right: -130px;
    padding: 11px;
    min-width: 331px !important;
    max-width: 327px !important;
  }
  .add{
	display:inline;
  }
  .add2{
	margin-left: 0px;
   }
   .search-border1 input{
	   width:100%;
   }
}
</style>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
   <?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Sainik Details</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Sainik </li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-12">
           

            <div class="card">
              <div class="card-header">
                <h3 class="card-title">Sainik List</h3>
              </div>
              <!-- /.card-header -->
			  <div class="mailbox-controls">
                <!-- Check all button -->
               
                <div class="btn-group">
				  <?php if(in_array('View',$action_ar)){ ?>
                  <button type="button" onclick="view_profile()" class="btn btn-default btn-sm"><i class="far fas fa-eye"></i></button>
				   <?php } ?>
				  <?php if(in_array('Delete',$action_ar)){ ?>
                  <button type="button" onclick="delete_profile()"  class="btn btn-default btn-sm"><i class="far fa-trash-alt"></i></button>
				  <?php } ?>
				   <span class="btn btn-default" id="myFunction" onclick="myFunction()" style="font-size: 12px;" ><i class="fa fa-caret-down"></i>Search</span>
                   <span class="btn btn-default" style="display:none;font-size: 12px;" id="myFunction_close" onclick="myFunction_close()"><i class="fa fa-caret-down"></i>Search</span>
				     <div class="input-group input-group-sm" style="width: 74px;">
                   <!-- <input type="text" name="table_search" class="form-control float-right" placeholder="Search">!-->
                    <div class="input-group-append">
                     
					  <span class="nav-item show" id="srch_dr">
						<div class="dropdown-menu dropdown-menu-lg dropdown-menu-right show new_dr" >
						 
						  <div class="dropdown-divider"></div>
						 <!-- <span class="btn btn-default btn-sm">X</span>!-->
						   <div class="search-border">
						  <input type="text" placeholder="Army No" id="army_no" >
						 </div>
						  
						  <div class="search-border">
						  <input type="text" placeholder="Serving Unit" id="serving_unit" >
						 </div>
						 
						 <div class="search-border">
						  <input type="text" placeholder="Parent Unit" id="parent_unit" >
						 </div>
                            <div class="search-border">
						  <input type="text" placeholder="Family name" id="family_name">
						 </div>
                            
                            <div class="search-border">
						  <input type="text" placeholder="NOK name" id="nok_name" >
						 </div>
                        <div class="add" >
						 <span class="search-border1">
						  <input type="text" placeholder="State" id="state">
						 </span>
                            <span class="search-border1 add2">
						  <input type="text" placeholder="district" id="district">
						 </span>
						 </div>
                           <div class="add" >
						 <span class="search-border1">
						  <input type="text" placeholder="Tehsil" id="tehsil" >
						 </span>
                            <span class="search-border1 add2" >
						  <input type="text" placeholder="Post" id="post" >
						 </span>
						 </div> 
                           <div class="search-border">
						  <input type="text" placeholder="Pincode" id="pincode" style="border: none; width:100%;">
						 </div>      
						 <br>
						  <button type="submit" style="margin-top: 6px;" onclick="filter_member()" class="btn btn-primary btn-sm">Submit</button>
						
						</div>
					  </span>
                    </div>
                  </div>
                
               
                </div>
				                
                
              </div>
            <div class="card-body table-responsive p-0">
             
				<table id="dt-cell-sellection" class="table" cellspacing="0" width="100%" >
				  <thead>
					<tr>
									<th ></th>
									<th class="th-sm">Army no</th>
									<th class="th-sm">Rank</th>
									<th class="th-sm">Name</th>
									<th class="th-sm">Date of cas</th>
									<th class="th-sm">Serving Unit</th>
									<th class="th-sm">Parent Unit</th>
					</tr>
				  </thead>
				 <tbody id="table_data">
			  
				 </tbody>
				</table>

              </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
        </div>
              <!-- /.card-body -->
            </div>
            <!-- /.card -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div>
      <!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
 <?php include('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- DataTables -->
<script src="plugins/datatables/jquery.dataTables.min.js"></script>
<script src="plugins/datatables-bs4/js/dataTables.bootstrap4.min.js"></script>

<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="dist/js/demo.js"></script>
<script src="address_filter.js"></script>
<!-- page script -->
<script>
$(document).ready(function () {

});
$( document ).ready(function() {
	
	view_members();
	
});
function filter_member(){
	$('#table_data').html('');
		var dataString = 'type=filter_members&army_no='+$('#army_no').val()+'&serving_unit='+$('#serving_unit').val()+'&parent_unit='+$('#parent_unit').val()+'&family_name='+$('#family_name').val()+'&nok_name='+$('#nok_name').val()+'&state='+$('#state').val()+'&district='+$('#district').val()+'&tehsil='+$('#tehsil').val()+'&post='+$('#post').val()+'&pincode='+$('#pincode').val();
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
		
		       $("#srch_dr").css('display','none');
				$('#table_data').html(data);
				
		}
	});
}
function view_members(){
	
	var dataString = 'type=view_members';
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#table_data').html(data);
				if(data){
					$('#dt-cell-sellection').dataTable({

					select: {
					  style: 'os',
					  items: 'cell'
					}
				  });
				}
				
		}
	});
	$('input[type=checkbox]').prop("checked", false);
}
function select_member(id){
	 if($("#cn"+id).prop("checked") == true){
		$("#d"+id).css("background","#c5c0c0");
	 }
	 else{
		$("#d"+id).css("background","white"); 
	 }
}
function view_profile(){
	//var select_item='';
	var select_item=$('input[type=checkbox]:checked').map(function() {
    return this.value;
	}).get();
	
	var str=select_item.toString();
	var cn=str.split(",").length;
	if(select_item==''){
		alert('Please select a member');
	}
	else if(cn > 1){
		alert('Select any one for view');
	}
	else{
		location.href="view_profile.php?id="+select_item;
	}
	
}
function delete_profile(){
	var select_item=$('input[type=checkbox]:checked').map(function() {
    return this.value;
	}).get();
	if(select_item==''){
		alert('Please select a member');
	}
	else{
		if(confirm('Are you want to delete Selected items')==true){
		var dataString = 'id='+select_item+'&type=delete_profile';
		
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
			
				view_members();	
			}
		});
		}else{
			return false;
		}
		
	}
	
}
  
  
  


  $(document).ready(function(){
        // Show hide popover
        $("#myFunction").click(function(){
            $("#srch_dr").show("fast");
        });
    });
    $('#srch_dr').click(function(e) { 
  e.stopPropagation();
 })

$(document).click(function(){ 
            $("#srch_dr").hide() 
                   
    });


</script>
</body>
</html>
