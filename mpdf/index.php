<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
    <title>How to generate PDF file using PHP and MPDF | Mitrajit's Tech Blog</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.9/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    
         
    <style type="text/css">
        .mtb-margin-top { margin-top: 20px; }
        .top-margin { border-bottom:2px solid #ccc; margin-bottom:20px; display:block; font-size:1.3rem; line-height:1.7rem;}

        .demo-page-header {
            text-align: center;
            font-size: 17px;
            text-transform: uppercase;
        }
        .margin-top-bottom {
            margin:20px 0;
            border: 4px solid #ccc;
            border-radius: 5px;
            padding: 8px 0;
        }
        .error {
            text-align: center;
            font-size: 15px;
            color: #ff0000;
            margin-top: 10px;
        }
        @media screen and (max-width:300px) {
            p {font-size:11px;}
            h1.top-margin {font-size:15px; line-height: 1.5em;}
        }
        
    </style>
</head>
<body>
    
    <div class="container mtb-margin-top">
        <div class="row">
            <div class="col-md-12">
                <h1 class="top-margin">Read the full article -- <a href="http://www.mitrajit.com/how-to-generate-pdf-file-using-php-and-mpdf-library/" target="_blank" title="How to copy text to clipboard using jQuery">How to generate PDF file using PHP and MPDF</a> on <a href="http://www.mitrajit.com/" title="Mitrajit's Tech Blog">Mitrajit's Tech Blog</a></h1>
            </div>
        </div>
    </div>

    
    <div class="container">        
        <div class="row">   
            <div class="col-lg-12">
                <h1 class="demo-page-header">-------    Generate Dynamic PDF file using PHP and MPDF    -------</h1>
            </div>
        </div> 
        <form action="generate-pdf.php" method="post">
          <div class="form-row">
            <div class="form-group col-md-6">
              <label for="name">Name</label>
              <input type="text" class="form-control" id="name" name="name">
            </div>
            <div class="form-group col-md-6">
              <label for="email">Email</label>
              <input type="text" class="form-control" id="email" name="email">
            </div>
          </div>
          <div class="form-group">
            <label for="address">Address</label>
            <input type="text" class="form-control" id="address" name="address">
          </div>
          <div class="form-group">
            <label for="address">Profile Picture Link</label>
            <input type="text" class="form-control" id="picture_link" name="picture_link">
            <label>Example, http://www.mitrajit.com/wp-content/uploads/2019/02/mitrajit.jpg</label>
          </div>
          <button type="submit" class="btn btn-primary">Submit & Generate PDF</button>
        </form>
    </div><!-- .container -->
</body>
</html>