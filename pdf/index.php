<?Php


include('config.php');
  $qry="SELECT sambandh_parent.army_no,sambandh_parent.name as parent_name,sambandh_parent.rank,sambandh_parent.cas_dt,sambandh_parent.serving_unit,sambandh_nok.name as nok_name,sambandh_nok.gender as nok_gender,sambandh_nok.Salutation,sambandh_nok.custom_salutation,sambandh_nok.relation as nok_relation,sambandh_nok.dob as nok_dob,sambandh_nok.education as nok_education,sambandh_nok.address1,sambandh_nok.address2,sambandh_nok.thesil,sambandh_nok.pincode,sambandh_nok.post,sambandh_nok.district,sambandh_nok.state,sambandh_nok.contact1,sambandh_nok.bank_account,sambandh_nok.bank_name FROM sambandh_parent LEFT JOIN sambandh_nok ON sambandh_parent.army_no=sambandh_nok.army_no WHERE sambandh_parent.army_no='".$_GET['army_no']."'";
$run=mysqli_query($con,$qry);
while($out=mysqli_fetch_assoc($run)){
	$parent_name=$out['parent_name'];
	$cas_dt=$out['cas_dt'];
	$army_no=$out['army_no'];
	$rank=$out['rank'];
	$serving_unit=$out['serving_unit'];
	$nok_name=$out['nok_name'];
	$nok_gender=$out['nok_gender'];
	$Salutation=$out['Salutation'];
	$nok_relation=$out['nok_relation'];
	$nok_education=$out['nok_education'];
	$nok_dob=$out['nok_dob'];
	$address1=$out['address1'];
	$address2=$out['address2'];
	$thesil=$out['thesil'];
	$pincode=$out['pincode'];
	$post=$out['post'];
	$district=$out['district'];
	$state=$out['state'];
	$contact1=$out['contact1'];
	$bank_account=$out['bank_account'];
	$bank_name=$out['bank_name'];
	if($bank_account==0){$bank_account='';}
	if($pincode==0){$pincode='';}
	if($contact1==0){$contact1='';}
	if($cas_dt=='0000-00-00'){$cas_dt='';}
	if($Salutation=='Others'){$Salutation=$out['custom_salutation'];}
	
}

require('fpdf.php');
$pdf = new FPDF(); 
$pdf->AddPage();
$pdf->Cell(10);
$pdf->SetFont('Arial','',12);
$pdf->Multicell(0,2,"Rehabilitation and Welfare Section\n\n
Directorate of Indian Army Veterans (DIAV)\n\n
Adjutant Generals Branch\n\n
Integrated Headquarters of Ministry of Defence (Army)\n\n
Adjacent to Central Org ECHS\n\n
104 Cavalry Road, Delhi Cantt -110010\n\n\n\n"); 
$pdf->SetFont('Arial','B',13);
$pdf->Cell(10);
$pdf->Cell(40,5,'APPLICATION FORM FOR EDUCATION SCHOLARSHIP SCHEME FOR THE ','C');
$pdf->Ln();
$pdf->SetFont('Arial','B',13);
$pdf->Cell(60);
$pdf->Cell(40,5,'ACADEMIC YEAR 20__/20__','C');
$pdf->Ln();
$pdf->SetFont('Arial','B',12);
$pdf->Cell(60);
$pdf->Cell(40,5,'ONE TIME COMPUTER GRANT','C');
$pdf->Ln();
$pdf->SetFont('Arial','',10);
$pdf->Cell(20);
$pdf->Cell(40,5,'NOTE:PLEASE DO NOT LEAVE ANY INFORMATION BLANK (USE BLOCK LETTERS)','C');
$pdf->Ln();
$pdf->Ln();


$pdf->SetFont('Arial','',11);
$pdf->Cell(85);
$pdf->Cell(40,5,'PART 1','C');
$pdf->Ln();
$pdf->SetFont('Arial','B',10.5);
$pdf->Cell(10);
$pdf->Cell(40,5,'1) Personal Particulars of Soldier','C');
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Army Number',1,0,'L');
$pdf->Cell(80 ,6,$army_no,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Rank',1,0,'L');
$pdf->Cell(80 ,6,$rank,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Name',1,0,'L');
$pdf->Cell(80 ,6,$parent_name,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Regiment/Unit',1,0,'L');
$pdf->Cell(80 ,6,$serving_unit,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Date of Death/Casualty',1,0,'L');
$pdf->Cell(80 ,6,$cas_dt,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Nature of Casualty',1,0,'L');
$pdf->Cell(80 ,6,'',1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Name of NOK',1,0,'L');
$pdf->Cell(80 ,6,$nok_name,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(150 ,6,'Address of NOK',1,1,'L');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Address 1',1,0,'L');
$pdf->Cell(80 ,6,$address1,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Address 2',1,0,'L');
$pdf->Cell(80 ,6,$address2,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Village',1,0,'L');
$pdf->Cell(80 ,6,'',1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Tehsil',1,0,'L');
$pdf->Cell(80 ,6,$thesil,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Post',1,0,'L');
$pdf->Cell(80 ,6,$post,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'District',1,0,'L');
$pdf->Cell(80 ,6,$district,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'State',1,0,'L');
$pdf->Cell(80 ,6,$state,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Pincode',1,0,'L');
$pdf->Cell(80 ,6,$pincode,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Telephone',1,0,'L');
$pdf->Cell(80 ,6,$contact1,1,1,'C');
$pdf->Ln();
$pdf->Ln();
//////////////////////childrens details
$pdf->SetFont('Arial','',11);
$pdf->Cell(85);
$pdf->Cell(40,5,'PART 2','C');
$pdf->Ln();
$pdf->SetFont('Arial','B',10.5);
$pdf->Cell(10);
$pdf->Cell(40,5,'2) Particulars of the Child','C');
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Name of Child',1,0,'L');
if($nok_relation=='SON' || $nok_relation=='DAUGHTER'){
	$pdf->Cell(80 ,6,$nok_name,1,1,'C');
}else{
	$pdf->Cell(80 ,6,'',1,1,'C');
}
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Sex',1,0,'L');
if($nok_relation=='SON' || $nok_relation=='DAUGHTER'){
$pdf->Cell(80 ,6,$nok_gender,1,1,'C');	
}else{
$pdf->Cell(80 ,6,'',1,1,'C');
}
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Name',1,0,'L');
if($nok_relation=='SON' || $nok_relation=='DAUGHTER'){
	$pdf->Cell(80 ,6,$nok_name,1,1,'C');
}else{
	$pdf->Cell(80 ,6,'',1,1,'C');
}$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Relationship',1,0,'L');
if($nok_relation=='SON' || $nok_relation=='DAUGHTER'){
	$pdf->Cell(80 ,6,$nok_relation,1,1,'C');
}else{
	$pdf->Cell(80 ,6,'',1,1,'C');
}$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Date of Birth',1,0,'L');
if($nok_relation=='SON' || $nok_relation=='DAUGHTER'){
	$pdf->Cell(80 ,6,$nok_dob,1,1,'C');
}else{
	$pdf->Cell(80 ,6,'',1,1,'C');
}$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Class Passed',1,0,'L');
if($nok_relation=='SON' || $nok_relation=='DAUGHTER'){
	$pdf->Cell(80 ,6,$nok_education,1,1,'C');
}else{
	$pdf->Cell(80 ,6,'',1,1,'C');
}$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(70 ,6,'Percentage of Marks',1,0,'L');
$pdf->Cell(80 ,6,'',1,1,'C');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
///////////bank details///////////////////
$pdf->SetFont('Arial','',11);
$pdf->Cell(85);
$pdf->Cell(40,5,'PART 3','C');
$pdf->Ln();
$pdf->SetFont('Arial','B',10.5);
$pdf->Cell(10);
$pdf->Cell(40,5,'3) Bankers Details','C');
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'Account Number',1,0,'L');
$pdf->Cell(70 ,6,$bank_account,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'IFS Code',1,0,'L');
$pdf->Cell(70 ,6,'',1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'Name of Bank',1,0,'L');
$pdf->Cell(70 ,6,$bank_name,1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,12,'Address of Bank ',1,0,'L');
$pdf->Cell(70 ,12,'',1,1,'C');
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'Name of Account Holder (As per Bank Records)',1,0,'L');
$pdf->Cell(70 ,6,'',1,1,'C');
$pdf->SetFont('Arial','B',10);
$pdf->Cell(20);
$pdf->Cell(40,5,'(Attach cancelled cheque/copy of front page of bank passbook)','C');


$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'Date : ',0,0,'L');
$pdf->Cell(70 ,6,'(Name and Signature of the soldier/NOK)',0,0,'C');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();



$pdf->SetFont('Arial','B',12);
$pdf->Cell(50);
$pdf->Cell(40,5,'CERTIFICATE FROM SCHOOL','C');
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();


$pdf->SetFont('Arial','',12);
$pdf->Cell(10);
$pdf->Multicell(0,2,"Certified that Miss/Master __________________Son/Daughter of  ___________________\n\n 
is a bonafide student of class/course ___________________ in this school/institute\n\n 
 during the academic year ________________."); 
 
 
 $pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Ln();
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'Station : ',0,0,'L');
$pdf->Cell(70 ,6,'',0,0,'C');

$pdf->Ln();
$pdf->Cell(20);
$pdf->SetFont('Arial','',10);
$pdf->Cell(80 ,6,'Date : ',0,0,'L');
$pdf->Cell(70 ,6,'(Signature of Principal))',0,0,'C');


//$pdf->Output($army_no.'.pdf','D'); 
$pdf->Output('DIAVEDNFORM_'.$army_no.'_'.$nok_name.'.pdf','D'); 

?>