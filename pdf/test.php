<?Php
date_default_timezone_set("Asia/Kolkata");
  INCLUDE('config.php');
    if($_GET['Criteria']=='Equal'){
	$where="WHERE ".$_GET['Field']."='".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Not_Equal'){
	$where="WHERE ".$_GET['Field']." !=   '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Contains'){
	$where="WHERE ".$_GET['Field']." LIKE  '%".$_GET['Value']."%'";
	}
	elseif($_GET['Criteria']=='Begins_with'){
	$where="WHERE ".$_GET['Field']." LIKE  '".$_GET['Value']."%'";
	}
	elseif($_GET['Criteria']=='Ends_with'){
	$where="WHERE ".$_GET['Field']." LIKE  '%".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Greater_than'){
	$where="WHERE ".$_GET['Field']." >  '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Greater_than_or_Equal_to'){
	$where="WHERE ".$_GET['Field']." >=  '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Less_than'){
	$where="WHERE ".$_GET['Field']." <  '".$_GET['Value']."'";
	}
	elseif($_GET['Criteria']=='Less_than_or_Equal_to'){
	$where="WHERE ".$_GET['Field']." <=  '".$_GET['Value']."'";
	}
	////////
	if($_GET['Module']=='Sainik'){
		$ARMY_NO_QUERY="SELECT army_no FROM sambandh_parent ".$where." ";
	}
	elseif($_GET['Module']=='Nok'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_nok`  ".$where." group by army_no";
	}
	elseif($_GET['Module']=='Family'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_family`  ".$where." group by army_no";
	}
	elseif($_GET['Module']=='Unit'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_unit`  ".$where." group by army_no";
	}
	elseif($_GET['Module']=='Others'){
		 $ARMY_NO_QUERY="SELECT army_no FROM `sambandh_others`  ".$where." group by army_no";
	}
	$run=mysqli_query($con,$ARMY_NO_QUERY);
	if(mysqli_num_rows($run)>0){
		$array=array();
	while($out = mysqli_fetch_array($run)) {
		$array[]=$out['army_no'];
	}}
	
// print_r($array);
 //exit;


require('fpdf.php');
$pdf = new FPDF();

foreach($array as $army_no){ 
		$pdf->AddPage();
		$pdf->SetFont('Arial','',12);
		//$pdf->Multicell(0,2,"Rehabilitation and Welfare Section\n\n
		// Directorate of Indian Army Veterans (DIAV)\n\n
		// Adjutant Generals Branch\n\n
		// Integrated Headquarters of Ministry of Defence (Army)\n\n
		// Adjacent to Central Org ECHS\n\n
		// 104 Cavalry Road, Delhi Cantt -110010\n\n\n\n"); 
		$pdf->SetFont('Arial','B',13);
		$pdf->Cell(80);
		$pdf->Cell(40,5,'SAMBANDH','C');
		$pdf->Ln();

		$pdf->SetFont('Arial','',11);
		$pdf->Cell(10);
		$pdf->Cell(20,5,'_____________________________________________________________________________','C');
		$pdf->Ln();
		$pdf->Ln();

		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(40,5,'1) SAINIK','C');
		$pdf->Ln();
		$pdf->Ln();
		$result = mysqli_query($con,"SELECT * FROM `sambandh_parent`  WHERE army_no =".$army_no."");
		if(mysqli_num_rows($result)>0){
			while($row = mysqli_fetch_array($result)) {
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Army No','C');
				$pdf->Cell(50,5,': ' .$row['army_no'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Rank','C');
				$pdf->Cell(40,5,': '.$row['rank'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Name','C');
				$pdf->Cell(40,5,': '.$row['name'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Date of Cas','C');
				$pdf->Cell(40,5,': '.$row['cas_dt'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Serving Unit','C');
				$pdf->Cell(40,5,': '.$row['serving_unit'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Parent Unit','C');
				$pdf->Cell(40,5,': '.$row['parent_unit'],0,0,'L');
				$pdf->Ln();
				$pdf->Ln();
			}
		}
//-----------------------------NOK-------------------------
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(40,5,'2) NOK','C');
		$pdf->Ln();
		$pdf->Ln();
		
		$result = mysqli_query($con,"SELECT * FROM `sambandh_nok`  WHERE army_no =".$army_no."");
		if(mysqli_num_rows($result)>0){
			while($row= mysqli_fetch_array($result)) {
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Name','C');
				$pdf->Cell(50,5,': '.$row['name'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Relation','C');
				$pdf->Cell(40,5,': '.$row['relation'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Address 1','C');
				$pdf->Cell(40,5,': '.$row['address1'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Address 2','C');
				$pdf->Cell(40,5,': '.$row['address2'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Thesil','C');
				$pdf->Cell(40,5,': '.$row['thesil'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'District','C');
				$pdf->Cell(40,5,': '.$row['district'],0,0,'L');
				$pdf->Ln(); 
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Post','C');
				$pdf->Cell(50,5,': '.$row['post'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'State','C');
				$pdf->Cell(50,5,': '.$row['state'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Pincode','C');
				$pdf->Cell(50,5,': '.$row['pincode'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Contact1','C');
				$pdf->Cell(50,5,': '.$row['contact1'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Contact2','C');
				$pdf->Cell(50,5,': '.$row['contact2'],0,0,'L');
				$pdf->Ln();
				$pdf->Ln();
			}
		}
		else{
			$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Name','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Relation','C');
				$pdf->Cell(40,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Address 1','C');
				$pdf->Cell(40,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Address 2','C');
				$pdf->Cell(40,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Thesil','C');
				$pdf->Cell(40,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'District','C');
				$pdf->Cell(40,5,': ',0,0,'L');
				$pdf->Ln(); 
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Post','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'State','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Pincode','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Contact1','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Contact2','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Ln();
		}
//--------------------------------------FAMILY
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(40,5,'3) FAMILY','C');
		$pdf->Ln();
		$pdf->Ln();

		$result = mysqli_query($con,"SELECT * FROM `sambandh_family`  WHERE army_no =".$army_no."");
		$n=1;
		if(mysqli_num_rows($result)>0){
				while($row= mysqli_fetch_array($result)) {
				$pdf->SetFont('Arial','B',9);
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Member '.$n,'C');
				$pdf->Ln();
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Name','C');
				$pdf->Cell(50,5,': '.$row['name'],0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Gender','C');
				$pdf->Cell(50,5,': '.$row['gender'],0,0,'L');
				$pdf->Ln();     
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Relation','C');
				$pdf->Cell(50,5,': '.$row['relation'],0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(20,5,'DOB','C');
				$pdf->Cell(50,5,': '.$row['dob'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Contact 1','C');
				$pdf->Cell(50,5,': '.$row['contact1'],0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Contact 2','C');
				$pdf->Cell(50,5,': '.$row['contact2'],0,0,'L');
				$pdf->Ln();  
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Education','C');
				$pdf->Cell(50,5,': '.$row['education'],0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Education Year','C');
				$pdf->Cell(50,5,': '.$row['education_year'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(20,5,'_____________________________________________________________________________','C');
				$pdf->Ln(); 
				
				$n++;
			}
		}
		else{
			$pdf->SetFont('Arial','B',9);	
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Name','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Gender','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();     
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Relation','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(20,5,'DOB','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Contact 1','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Contact 2','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();  
				$pdf->Cell(20);
				$pdf->Cell(20,5,'Education','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Cell(20);
				$pdf->Cell(30,5,'Education Year','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
		}
//-------------------------UNIT--------------------------				
		$pdf->Ln(); 
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(40,5,'4) UNIT','C');
		$pdf->Ln();
		$pdf->Ln();

		$result = mysqli_query($con,"SELECT * FROM `sambandh_unit`  WHERE army_no =".$army_no."");
		if(mysqli_num_rows($result)>0){
			while($row= mysqli_fetch_array($result)) {
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Unit / Organization','C');
				$pdf->Cell(50,5,': '.$row['unit_org'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Contact 1','C');
				$pdf->Cell(50,5,': '.$row['contact1'],0,0,'L');
				$pdf->Ln();     
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Email','C');
				$pdf->Cell(50,5,': '.$row['contact2'],0,0,'L');
				$pdf->Ln();  
				$pdf->Cell(20);
				$pdf->Cell(20,5,'_____________________________________________________________________________','C');
				$pdf->Ln(); 
			}
		}
		else{
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Unit / Organization','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Contact 1','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();     
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Email','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln(); 
		}
//--------------------------OTHERS---------------------------		
		$pdf->Ln(); 
		$pdf->SetFont('Arial','B',10);
		$pdf->Cell(10);
		$pdf->Cell(40,5,'5) OTHERS','C');
		$pdf->Ln();
		$pdf->Ln();
		$result = mysqli_query($con,"SELECT * FROM `sambandh_others`  WHERE army_no =".$army_no."");
		if(mysqli_num_rows($result)>0){
			while($row= mysqli_fetch_array($result)) {
				
				$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Name','C');
				$pdf->Cell(50,5,': '.$row['name'],0,0,'L'); 
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Unit / Organization','C');
				$pdf->Cell(50,5,': '.$row['unit_org'],0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Contact 1','C');
				$pdf->Cell(50,5,': '.$row['contact1'],0,0,'L');
				$pdf->Ln();     
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Email','C');
				$pdf->Cell(50,5,': '.$row['email'],0,0,'L');
				$pdf->Ln(); 
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Remarks','C');
				$pdf->Cell(50,5,': '.$row['remarks'],0,0,'L'); 
				$pdf->Ln();  
				$pdf->Cell(20);
				$pdf->Cell(20,5,'_____________________________________________________________________________','C');
				$pdf->Ln();
			}
		}
		else{
			$pdf->SetFont('Arial','',10);
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Name','C');
				$pdf->Cell(50,5,': ',0,0,'L'); 
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Unit / Organization','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Contact 1','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln();     
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Email','C');
				$pdf->Cell(50,5,': ',0,0,'L');
				$pdf->Ln(); 
				$pdf->Cell(20);
				$pdf->Cell(40,5,'Remarks','C');
				$pdf->Cell(50,5,': ',0,0,'L'); 
				$pdf->Ln();  
				
		}

    }	
$date=date("d-m-Y h:i:s");

$pdf->Output('Query_'.$date.'.pdf','D'); 
?>