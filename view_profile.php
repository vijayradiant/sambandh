<?php 
session_start();
if(!isset($_SESSION['name'])){
		header('location:login.php');
}
?>
<!DOCTYPE html>
<html>
<?php INCLUDE('head.php');?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
 <?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
   <?PHP INCLUDE('sidebar.php'); ?>
<style>
.autocomplete-items {
  position: absolute;
  border: 1px solid #d4d4d4;
  border-bottom: none;
  border-top: none;
  z-index: 99;
  /*position the autocomplete items to be the same width as the container:*/
  top: 100%;
     left: 50%;
      max-height: 300px;
    overflow-y: auto;
	width: 50%;
}

.autocomplete-items div {
    padding: 10px;
    cursor: pointer;
    background-color: #fff;
    border-bottom: 1px solid #d4d4d4;
}
.custom_address{
	width: 100%;
    margin-top: 2px;
    border: 1px solid #1E88E5;
	background:#f4f5f7;
}
.img_fam{
	width:50%;
}
.panel .col-6{
	margin-top:10px;
}
.panel {
 
  display: none;
 background: #f1f1f1;
  overflow: hidden;
}
.accordion{
	display:inline-flex;
}
.modal {
  display: none; /* Hidden by default */
  position: fixed; /* Stay in place */
  z-index: 1; /* Sit on top */
  padding-top: 100px; /* Location of the box */
  left: 0;
  top: 0;
  width: 100%; /* Full width */
  height: 100%; /* Full height */
  overflow: auto; /* Enable scroll if needed */
  background-color: rgb(0,0,0); /* Fallback color */
  background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
}

/* Modal Content */
.modal-content {
  background-color: #fefefe;
  margin: auto;
  padding: 20px;
  border: 1px solid #888;
  width: 55%;
}

/* The Close Button */
.close {
  color: #aaaaaa;
  float: right;
  font-size: 28px;
  font-weight: bold;
}

.close:hover,
.close:focus {
  color: #000;
  text-decoration: none;
  cursor: pointer;
}
.dateselector{
	    z-index: 1;
}
#cont_min,#cont_max{
	display:none;
	
}
@media screen and (max-width: 500px) {
#cont_max{
	display:inline;
	
}
}
#min_nok,#min_unit,#min_fam,#show_fam,#show_nok,#addunit,#update_nok,#show_unit_form,#show_others_form,#min_others,#nok_dox_type_define,#dox,#family_dox_type_define,#dox1,#dox_type_define_1,#nok_po_custom,#nok_district_custom,#nok_state_custom,#nok_tehsil_custom,#nok_pincode_custom,#family_po_custom,#family_district_custom,#family_state_custom,#family_tehsil_custom,#family_pincode_custom{
	display:none;
}
#min_UN,#add_UN{
	color:blue;
}
.success_parent{
	display:none;
	background: #43A047;
    margin-top: 4px;
    padding: 2px;
    color: white;
    border-radius: 4px;
}
.float-right{
	   
    width: 50%;
}
.select2-container{
	    float: right;
    width: 100% !important;
}
.select2-container--default .select2-selection--single .select2-selection__rendered {
    color: #444;
    line-height: 20px !important;
}
.select2-container--default .select2-selection--single {
    background-color: #fff;
    border: 1px solid #aaaaaa7a;
    border-radius: 4px;
    height: 31px !important;
}
#doc_but{
	border: none;
    background: greenyellow;
    font-size: 11px;
    padding-left: 10px;
    padding-right: 10px;
   
}

.custom-file-input {
  color: transparent;
}
.custom-file-input::-webkit-file-upload-button {
  visibility: hidden;
}
.custom-file-input::before {
  content: '+ Add Document';
  color: black;
  display: inline-block;
  background: -webkit-linear-gradient(top, #f9f9f9, #e3e3e3);
  border: 1px solid #999;
  border-radius: 3px;
  padding: 0px 5px;
  outline: none;
  white-space: nowrap;
  -webkit-user-select: none;
  cursor: pointer;
  text-shadow: 1px 1px #fff;
  font-weight: 700;
  font-size: 10pt;
  opacity: 1000 !important;
}
.custom-file-input:hover::before {
  border-color: black;
}
.custom-file-input:active {
  outline: 0;
}
.custom-file-input:active::before {
  background: -webkit-linear-gradient(top, #e3e3e3, #f9f9f9); 
}
.custom-file-input{
	opacity: 1 !important;
}
#define_rel,#nok_define_rel,#nok_mrg_date,#family_mrg_date,#cust_fam,#cust_nok{
	background: #f5deb3b0;
	display:none;
}
.cust,#mr_status{
	display:none;
	
}
.dox,.dox1{
	float: right;
    padding: 8px;
}
.dox input,.dox1 input{
	margin-left: 0px;
    margin-right: 1px;
	border: 1px solid #8080804d;min-width: 180px;padding: 2px;
	margin-bottom: 4px;
}
.dox select,.dox1 select {
	border: 1px solid #8080804d;min-width: 180px;padding: 2px;
	margin-bottom: 4px;
}
#custom_salutation_others{
	border: 1px solid red
}
.the-datepicker__deselect-button {
    text-decoration: none;
    color: #007eff;
    font-weight: bold;
    display: none;
}
#sd .the-datepicker__container{
	margin: 6px;
    right:167px;
}
@media screen and (max-width: 768px) {
#sd .the-datepicker__container{
	margin: 6px;
    right: 256px;
}
#sd .the-datepicker__main{
	margin: 6px;
    left: 4px !important;
}	
}
@media screen and (max-width: 768px) {
#sd1 .the-datepicker__container{
	margin: 6px;
    right: 256px;
}
#sd1 .the-datepicker__main{
	margin: 6px;
    left: 4px !important;
}
.modal-content{
	width:100%;
}
.panel .col-6{
	max-width:100%;
}
.img_fam{
	width:85%;
}	
}

</style>
  <!-- Content Wrapper. Contains page content -->
 
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Profile</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php">Home</a></li>
              <li class="breadcrumb-item active">Member Profile</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <div class="col-md-5" id="sd1">
			 <section class="content">
				  <div class="container-fluid">
					<div class="row">
					  <div class="col-12">
							<div id="accordion1">
								<div class="card">
									<a class="card-link" data-toggle="collapse" href="#collapseTwo">
									  <div class="card-header">
										 <h3 class="card-title">Profile</h3> 
										
									  </div>
									</a>
								  <div id="collapseTwo" class="collapse show" data-parent="#accordion1">
									 <div class="card-body">
										<div class="card card-primary card-outline">
							
											<div class="card-body box-profile">
												<span id="profile_parent_data"></span>
												
												 <span style="color:red;display:none;" id="error">** Please Fill mandatory  field</span>
												<div class="alert_parent success_parent" >
												  <span class="closebtn">&times;</span>  
												  <strong>Success!</strong> <span id="success_parent"></span>
												</div>
											</div>
             
										</div>
									 </div>
									</div>
								</div>
							</div>
						
						<!-- /.card -->
					  </div>
					</div>
				  </div>
				</section>
         
          </div>
          <!-- /.col -->
          <div class="col-md-7" id="sd">
				<div id="accordion">
				<div class="card">
				 <a class="card-link" data-toggle="collapse" href="#collapseOne">
				  <div class="card-header">
					 <h3 class="card-title">Contact</h3> 
				   
				  </div>
				   </a>
				  <div id="collapseOne" class="collapse show" data-parent="#accordion">
					<div class="card-body">
					 <div class="card-header p-2">
							<ul class="nav nav-pills">
							  <li class="nav-item"><a class="nav-link active" href="#nok" data-toggle="tab">NOK</a></li>
							  <li class="nav-item"><a class="nav-link " href="#settings" data-toggle="tab">Family</a></li>
							  <li class="nav-item"><a class="nav-link" href="#unit" data-toggle="tab">Unit</a></li>
							  <li class="nav-item"><a class="nav-link " href="#others" data-toggle="tab">Others</a></li>
							</ul>
							


					  </div>
					  <div class="tab-content">
							  <div class=" tab-pane" id="account">
								<!-- Post -->
							   <span id="table_data_account"></span>
							  </div>
							  <!-- /.tab-pane -->
							  <div class="tab-pane" id="unit">
									<?php if(in_array('Add',$action_ar)){ ?>
									<div style="padding:10px;color:blue;">
										<i class="fas fa-plus" id="max_unit" onclick="max_unit()">Add unit</i>
										<i id="min_unit" class="fas fa-minus" onclick="min_unit()">Add unit</i>
									</div>	
									<?php } ?>	
									<div class="card-body p-0" id="show_unit_form">
									
									<span class="form-horizontal">
									 <form name="add_unit_form" action="">
									  <!--<div class="form-group row">
									  <input type="hidden" id="table_id_unit" name="table_id_unit" value="">
										<label for="inputName" class="col-sm-4 col-form-label">Rank</label>
										<div class="col-sm-8">
										 <select id="rank_unit" name="rank_unit" class="form-control float-right select2  "  >
											
										  </select>
										</div>
									  </div>!-->
									   <div class="form-group row">
									  <input type="hidden" id="table_id_unit" name="table_id_unit" value="">
										<label for="inputName" class="col-sm-4 col-form-label">Salutation</label>
										<div class="col-sm-8">
										 <select id="Salutation_unit" name="Salutation_unit" class="form-control float-right select2 select2-danger "  >
											
										  </select>
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputEmail" class="col-sm-4 col-form-label">Name</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control unit_data"  id="name_unit" name="name_unit" value="" placeholder=" Unit name">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputName2" class="col-sm-4 col-form-label">Appointment</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control unit_data" name="appointment_unit" id="appointment_unit" value="" placeholder="Appointment">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputName2" class="col-sm-4 col-form-label">Unit/Org</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control unit_data" name="org_unit" id="org_unit" value="" placeholder="Unit/Org">
										</div>
									  </div>
									  <div class="form-group row">
									   
										<label for="inputName2" class="col-sm-4 col-form-label">Contact No 1</label>
										<div class="col-sm-8">
										 <p style="color:red;display:none;" id="error_unit_contact1">** Mobile Number should be 10 Character </p>
										  <input type="text" class="form-control unit_data" name="contact1_unit" id="contact1_unit" value="" placeholder="Contact Number">
										</div>
									  </div>
									   <div class="form-group row">
									    
										<label for="inputName2" class="col-sm-4 col-form-label">Contact No 2</label>
										<div class="col-sm-8">
										 <p style="color:red;display:none;" id="error_unit_contact2">** Mobile Number should be 10 Character </p>
										  <input type="text" class="form-control unit_data" name="contact2_unit" id="contact2_unit" value="" placeholder="Contact Number">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputName2" class="col-sm-4 col-form-label">Email</label>
										<div class="col-sm-8">
										<p style="color:red;display:none;" id="error_unit_email">** wrong  </p>
										  <input type="text" class="form-control unit_data" name="email_unit" id="email_unit" value="" placeholder="Email">
										</div>
									  </div>
									  
									  <div class="form-group row">
										<label for="inputExperience" class="col-sm-4 col-form-label">Remarks</label>
										<div class="col-sm-8">
										  <textarea class="form-control unit_data"  id="remarks_unit" name="remarks_unit" placeholder=""></textarea>
										</div>
									  </div>
									 
									  </form>
									  <div class="form-group row">
										<div class="offset-sm-2 col-sm-10">
										  <button type="submit"  onclick="add_unit()" id="add_unit_but"  class="btn btn-danger unit_data_update">Save</button>
										  <button type="submit" style="display:none;" id="update_unit_but" onclick="update_unit()"  class="btn btn-danger unit_data_update">Update</button>
										  <span id="error_unit" style="color:red;"></span>
										</div>
									  </div>
									</span>
									
								  </div>
								
									<div class="alert2 success2" style="display:none;">
										<span class="closebtn">&times;</span>  
										<strong>Success!</strong> <span id="success2"></span>
									</div>	
									<div class="card-body table-responsive p-0">
									<hr>
										<table class="table table-hover text-nowrap">
										  <thead>
											<tr>
											  <th>Salutation</th>
											  <th>Name</th>
											  <th>Appointment</th>
											  <th>Unit/Org</th>
											  <th>Contact</th>
											  <th>Email</th>
											  <th>Action</th>
											</tr>
										  </thead>
										  <tbody id="table_data_unit">
										  </tbody>
										</table>
								  </div>
							  </div>
							  <div class="tab-pane" id="others">
									<?php if(in_array('Add',$action_ar)){ ?>
									<div style="padding:10px;color:blue;">
										<i class="fas fa-plus" id="max_others" onclick="max_others()">Add Other Members</i>
										<i id="min_others" class="fas fa-minus" onclick="min_others()">Add Other Members</i>
									</div>	
									<?php } ?>	
									<div class="card-body p-0" id="show_others_form">
									
									<span class="form-horizontal">
									 <form name="add_others_form" action="">
									 <input type="hidden" id="table_id_others" name="table_id_others" value="">
									 <!-- <div class="form-group row">
									  
										<label for="inputName" class="col-sm-4 col-form-label">Rank</label>
										<div class="col-sm-8">
										 <select id="rank_others" name="rank_others" class="form-control float-right select2 select2-danger basic_profile"  >
											
										  </select>
										</div>
									  </div>!-->
									   <div class="form-group row">
									 
										<label for="inputName" class="col-sm-4 col-form-label">Salutation</label>
										<div class="col-sm-8">
										 <select id="Salutation_others" onchange="custom_sal_others(this.value)" name="Salutation_others" class="form-control float-right   "  >
										        <option value=""   >Select Salutation</option>
												<option value="MR">MR</option>
												<option value="MRS">MRS</option>
												<option value="SHRI">SHRI</option>
												<option value="SMT">SMT</option>
												<option value="SMT">MS</option>
												<option value="Custom">CUSTOM FILLING</option>
										  </select>
										</div>
									  </div>
									  <div class="form-group row cust">
									 
										<label for="inputName" class="col-sm-4 col-form-label"></label>
										<div class="col-sm-8">
										
										<input type="text" class="form-control " name="custom_salutation_others" id="custom_salutation_others" value="" placeholder="Custom Filling">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputEmail" class="col-sm-4 col-form-label">Name</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control unit_data"  id="name_others" name="name_others" value="" placeholder=" Unit name">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputName2" class="col-sm-4 col-form-label">Appointment</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control unit_data" name="appointment_others" id="appointment_others" value="" placeholder="Appointment">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputName2" class="col-sm-4 col-form-label">Unit/Org</label>
										<div class="col-sm-8">
										  <input type="text" class="form-control unit_data" name="org_others" id="org_others" value="" placeholder="Unit/Org">
										</div>
									  </div>
									  <div class="form-group row">
									  
										<label for="inputName2" class="col-sm-4 col-form-label">Contact No 1</label>
										<div class="col-sm-8">
										<p style="color:red;display:none;" id="error_others_contact1">** Mobile Number should be 10 Character </p>
										  <input type="text" class="form-control unit_data" name="contact1_others" id="contact1_others" value="" placeholder="Contact Number">
										</div>
									  </div>
									   <div class="form-group row">
									   
										<label for="inputName2" class="col-sm-4 col-form-label">Contact No 2</label>
										<div class="col-sm-8">
										<p style="color:red;display:none;" id="error_others_contact2">** Mobile Number should be 10 Character </p>
										  <input type="text" class="form-control unit_data" name="contact2_others" id="contact2_others" value="" placeholder="Contact Number">
										</div>
									  </div>
									  <div class="form-group row">
										<label for="inputName2" class="col-sm-4 col-form-label">Email</label>
										<div class="col-sm-8">
										<p style="color:red;display:none;" id="error_others_email">** wrong  </p>

										  <input type="text" class="form-control unit_data" name="email_others" id="email_others" value="" placeholder="Email">
										</div>
									  </div>
									  
									  <div class="form-group row">
										<label for="inputExperience" class="col-sm-4 col-form-label">Remarks</label>
										<div class="col-sm-8">
										  <textarea class="form-control unit_data"  id="remarks_others" name="remarks_others" value="" placeholder=""></textarea>
										</div>
									  </div>
									 
									  </form>
									  <div class="form-group row">
										<div class="offset-sm-2 col-sm-10">
										  <button type="submit"  onclick="add_others()" id="add_others_but"  class="btn btn-danger unit_data_update">Save </button>
										  <button type="submit" style="display:none;" id="update_others_but" onclick="update_others()"  class="btn btn-danger unit_data_update">Update</button>
										  <span id="error_others" style="color:red;"></span>
										</div>
									  </div>
									</span>
									
								  </div>
								
									<div class="alert2 success2" style="display:none;">
										<span class="closebtn">&times;</span>  
										<strong>Success!</strong> <span id="success2"></span>
									</div>	
									<div class="card-body table-responsive p-0">
									<hr>
										<table class="table table-hover text-nowrap">
										  <thead>
											<tr>
											  <th>Salutation</th>
											  <th>Name</th>
											  <th>Appointment</th>
											  <th>Unit/Org</th>
											  <th>Contact</th>
											  <th>Email</th>
											  <th>Action</th>
											</tr>
										  </thead>
										  <tbody id="table_data_others">
										  </tbody>
										</table>
								  </div>
							  </div>
							  <!-- /.tab-pane -->
			
							<div class="active tab-pane" id="nok">
									<?php if(in_array('Add',$action_ar)){ ?>
									<div style="padding:10px;color:blue;" id="nok_add_status">
										<i class="fas fa-plus" id="add1" onclick="add1()">Add NOK </i>
										<i id="min_nok" class="fas fa-minus" onclick="rme1()">Add NOK </i>
									</div>	
									<?php } ?>						
									<div class="alert2 success2" style="display:none;">
											  <span class="closebtn">&times;</span>  
											  <strong>Success!</strong> <span id="success2"></span>
									</div>				  
									<span id="show_nok">	 
										<form action="" name="nok_form_add" enctype= "multipart/form-data">
										  <ul class="list-group list-group-unbordered mb-3">  
											  
											   <li class="list-group-item">
											 
												<b>Salutation</b> 
												<select id="nok_Salutation" onchange="custom_sal_nok(this.value)" class="float-right" name="nok_Salutation" style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;border-radius:2px;">
														<option value=""   >Select Salutation</option>
														<option value="MR">MR</option>
														<option value="MRS">MRS</option>
														<option value="SHRI">SHRI</option>
														<option value="SMT">SMT</option>
														<option value="MS">MS</option>
														<option value="Custom">CUSTOM FILLING</option>
												</select>
											  </li>
											   <li class="list-group-item" id="cust_nok">
												 <input type="text" class="float-right" style="border: 1px solid gray;border-radius: 3px;" name="nok_custom_salutation" id="nok_custom_salutation" placeholder="Custom Filling">
											  </li>
											  <li class="list-group-item">
											  <input type="hidden" name="table_id_nok" id="table_id_nok">
												<b>Name</b> <input type="text" class="float-right " name="nok_name" id="nok_name" >
											  </li>
											<li class="list-group-item">
 												<b>Gender</b>
												<span class="float-right ">
													 <input type="radio"  name="nok_gender" value="Male"> Male
													 <input type="radio" name="nok_gender"  value="Female"> Female
													 <input type="radio"  name="nok_gender" value="Others"> Others 
												</span>                 
											 </li>
											   <li class="list-group-item">
												<b>Relation : </b> 
														<select id="nok_relation" class="float-right "  onchange="nok_relation_others(this.value)"  name="nok_relation" style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;">
														<option value=""   selected>Select relation</option>
														<option value="WIFE" id="nok_r_Wife">Wife</option>
														<option value="HUSBAND" id="nok_r_Husband">Husband</option>
														<option value="FATHER" id="nok_r_Father">Father</option>
														<option value="MOTHER" id="nok_r_Mother">Mother</option>
														<option value="DAUGHTER" id="nok_r_Daughter">Daughter</option>
														<option value="SON" id="nok_r_Son">Son</option>
														<option value="SISTER" id="nok_r_Sister">Sister</option>
														<option value="BROTHER" id="nok_r_vBrother">Brother</option>
														<option value="BROTHER IN LAW" id="nok_r_Brother_in_Law">Brother in Law</option>
														<option value="SISTER IN LAW" id="nok_r_Sister_in_Law">Sister in Law</option>
														<option value="OTHERS" id="nok_r_Others">Others</option>
														
														</select>
														
											  </li>
											  <li class="list-group-item" id="nok_mrg_date">
											   <b>Date of married</b>
												 <input type="text" class="float-right " style="border: 1px solid gray;border-radius: 3px;" name="nok_dom" id="nok_dom" placeholder="DD.MM.YYYY">
											  </li>
											  <li class="list-group-item" id="nok_define_rel">
												 <input type="text" class="float-right" style="border: 1px solid gray;border-radius: 3px;" name="nok_define_others_rel" id="nok_define_others_rel" placeholder="Define Relation">
											  </li>
											   <li class="list-group-item" >
														<b>DOB : </b> <input type="text" id="nok_dob" name="nok_dob" class="float-right unit_data dtpic" value="" style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;" placeholder="DD.MM.YYY">
											  </li>
											   <li class="list-group-item">
												<b>Address 1</b> <input type="text" class="float-right " name="nok_address1" id="nok_address1" value="">
											  </li>
											   <li class="list-group-item">
												<b>Address 2</b> <input type="text" class="float-right " name="nok_address2" id="nok_address2" value="">
											  </li>
											   <li class="list-group-item">
												<b>State</b>
												       
														 <input type="text"  id="nok_state" name="nok_state" class="float-right state_nok">
														
											  </li>
											  <li class="list-group-item">
												<b>District</b> 
												
													
													<input type="text"  id="nok_district" name="nok_district" class="float-right district_nok">
												
												
											  </li>
											  <li class="list-group-item">
												<b>Tehsil</b> 
												
													
													<input type="text"  id="nok_tehsil" name="nok_tehsil" class="float-right tehsil_nok">
												
											  </li>
											  <li class="list-group-item">
												<b>Post office</b>
												
													
													<input type="text"  id="nok_po" name="nok_po" class="float-right post_nok">
											
											  </li>
											  <li class="list-group-item">
												<b>Pin code</b>
												
												
													<input type="text"  id="nok_pincode" name="nok_pincode" class="float-right pincode_nok">
												
											  </li>
											  <li class="list-group-item">
											    <p style="color:red;display:none;" id="error_nok_contact1">** Mobile Number should be 10 Character </p>
												<b>Contact Number 1</b> <input type="text" class="float-right " name="nok_contact1" id="nok_contact1" value="">
											  </li>
											  <li class="list-group-item">
											   <p style="color:red;display:none;" id="error_nok_contact2">** Mobile Number should be 10 Character </p>
												<b>Contact Number 2</b> <input type="text" class="float-right " name="nok_contact2" id="nok_contact2" value="">
											  </li>
											  <li class="list-group-item">
												<b>PPO Number</b> <input type="text" class="float-right " name="nok_ppo" id="nok_ppo" value="">
											  </li>
											  <li class="list-group-item">
												<b>Bank Name</b> <input type="text" class="float-right " name="nok_bank_name" id="nok_bank_name" value="">
											  </li>
											  <li class="list-group-item">
												<b>Bank Account No</b> <input type="text" class="float-right " name="nok_bank_account" id="nok_bank_account" value="">
											  </li>
											  <li class="list-group-item">
												<b>Education Qualification</b> <input type="text" class="float-right " name="nok_education" id="nok_education" value="">
											  </li>
											   <li class="list-group-item">
												<b>Year of Educational Qualification</b> <select type="year" class="float-right " name="nok_education_year" id="nok_education_year"  style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;">
												<option value="0000">Select Year</option>
												</select>
											  </li>
											  <li class="list-group-item">
												<b>Vocation</b> <input type="text" class="float-right " name="nok_vocation" id="nok_vocation" value="">
											  </li>
											  <li class="list-group-item nok_dox">
												<b>Dox</b> <input type="file"  class="float-right " id="nok_dox" name="nok_dox" >
												<span class="dox" id="dox">
													<select id="nok_dox_type"  class="float-right" name="nok_dox_type" >
															<option value=""   selected >Type of dox</option>
															<option value="IAFY1940">IAFY1940</option>
															<option value="PPO">PPO</option>
															<option value="Marksheet">Marksheet</option>
															<option value="Feereceipt">Feereceipt</option>
															<option value="Passbook">Passbook</option>
															<option value="Cancelled Cheque">Cancelled Cheque</option>
															<option value="Others">Others</option>
													</select>
													 <input type="text" class="float-right " id="nok_dox_type_define" name="nok_dox_type_define" placeholder="Type of dox">
													 <input type="text" class="float-right" id="nok_dox_page_no" name="nok_dox_page_no" placeholder="Page no">
													 <select type="year" class="float-right " name="nok_dox_year" id="nok_dox_year" >
													<option value="0000">Year of dox</option>
													</select>
												</span>
											  </li>
											  <li class="list-group-item">
												<b>Note</b> <textarea  class="float-right " name="nok_note" id="nok_note"></textarea>
											  </li>
										  </ul>
										</form>
										  <div class="card-footer">
												  <div class="text-right">
												
													<a href="#" id="add_nok" class="btn btn-sm btn-primary" onclick="add_nok()">
													  <i class="fas fa-user"></i> Save 
													</a>
													<a href="#" id="update_nok" class="btn btn-sm btn-primary" onclick="update_nok()">
													  Update
													</a>
										  </div>
										 
										  </div>
										   <br>
									</span>
									<div class="col-12 col-sm-12 col-md-12" style="display:contents;" id="table_data_nok">
									 </div>
								
							</div>
							<div class="tab-pane" id="settings">
								<div class="">
									<?php if(in_array('Add',$action_ar)){ ?>
									<div style="padding:10px;color:blue;">
										<i class="fas fa-plus" id="add_fam" onclick="add()">Add Family Members</i>
										<i id="min_fam" class="fas fa-minus" onclick="rme()">Add Family Members</i>
									</div>
									<?php } ?>						
									<div class="alert2 success2" style="display:none;">
											  <span class="closebtn">&times;</span>  
											  <strong>Success!</strong> <span id="success_nok"></span>
									</div>				  
									<div id="show_fam">	 
										<form action="" name="family_form_add" enctype= "multipart/form-data">
										  <ul class="list-group list-group-unbordered mb-3">  
											 
											  <li class="list-group-item">
											 
												<b>Salutation</b> 
							
												<select id="family_Salutation" onchange="custom_sal_family(this.value)" class="float-right" name="family_Salutation" style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;border-radius:2px;">
												<option value=""   >Select Salutation</option>
												<option value="MR">MR</option>
												<option value="MRS">MRS</option>
												<option value="SHRI">SHRI</option>
												<option value="SMT">SMT</option>
												<option value="MS">MS</option>
												<option value="Custom">CUSTOM FILLING</option>
												</select>
											  </li>
											  <li class="list-group-item" id="cust_fam">
												 <input type="text" class="float-right" style="border: 1px solid gray;border-radius: 3px;" name="family_custom_salutation" id="family_custom_salutation" placeholder="Custom Filling">
											  </li>
											   <li class="list-group-item">
											  <input type="hidden" name="table_id_family" id="table_id_family">
												<b>Name</b> <input type="text" class="float-right " name="family_name" id="family_name" >
											  </li>
											  <li class="list-group-item">
 												<b>Gender</b>
												<span class="float-right ">
													 <input type="radio"  name="family_gender" value="Male"> Male
													 <input type="radio" id="family_gender"  value="Female" name="family_gender"> Female
													 <input type="radio"  name="family_gender" value="Others"> Others 
												</span>                 
											  </li>
											   <li class="list-group-item">
												<b>Relation : </b> 
														<select id="family_relation" class="float-right "  onchange="relation_others(this.value)" name="family_relation" style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;">
														<option value=""   selected>Select relation</option>
														<option value="WIFE" id="nok_r_Wife">Wife</option>
														<option value="HUSBAND" id="nok_r_Husband">Husband</option>
														<option value="FATHER" id="nok_r_Father">Father</option>
														<option value="MOTHER" id="nok_r_Mother">Mother</option>
														<option value="DAUGHTER" id="nok_r_Daughter">Daughter</option>
														<option value="SON" id="nok_r_Son">Son</option>
														<option value="SISTER" id="nok_r_Sister">Sister</option>
														<option value="BROTHER" id="nok_r_vBrother">Brother</option>
														<option value="BROTHER IN LAW" id="nok_r_Brother_in_Law">Brother in Law</option>
														<option value="SISTER IN LAW" id="nok_r_Sister_in_Law">Sister in Law</option>
														<option value="OTHERS" id="nok_r_Others">Others</option>
														</select>
											  </li>
											   <li class="list-group-item" id="define_rel">
												 <input type="text" class="float-right" style="border: 1px solid gray;border-radius: 3px;" name="family_define_others_rel" id="family_define_others_rel" placeholder="Define Relation">
											  </li>
											  <li class="list-group-item" id="mr_status">
												<b>Marital Status</b> 
												<span style="float:right;">
												MARRIED <input type="radio" value="MARRIED" name="family_mar_status">
												UNMARRIED <input type="radio"  value="UNMARRIED" name="family_mar_status">
												</span>
											  </li>
											    <li class="list-group-item" id="family_mrg_date">
											   <b>Date of married</b>
												 <input type="text" class="float-right " style="border: 1px solid gray;border-radius: 3px;" name="family_dom" id="family_dom" placeholder='DD.MM.YYYY' >
											  </li>
											   <li class="list-group-item">
														<b>DOB : </b> <input type="text" id="family_dob" name="family_dob" class="float-right unit_data " value="" style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;" placeholder='DD.MM.YYYY'>
											  </li>
											  
											   <li class="list-group-item">
												<b>Address 1</b> <input type="text" class="float-right " name="family_address1" id="family_address1" value="">
											  </li>
											   <li class="list-group-item">
												<b>Address 2</b> <input type="text" class="float-right " name="family_address2" id="family_address2" value="">
											  </li>
											 
											  
											   <li class="list-group-item">
												<b>State</b>
												
													
													<input type="text"  id="family_state" name="family_state" class="float-right">
											
											  </li>
											  <li class="list-group-item">
												<b>District</b>
												
													
													<input type="text"  id="family_district" name="family_district" class="float-right">
												
											  </li>
											   <li class="list-group-item">
												<b>Tehsil</b> 
												
													
													<input type="text"  id="family_tehsil" name="family_tehsil" class="float-right">
												
											  </li>
												 <li class="list-group-item">
												<b>Post</b>
												
												
													<input type="text"  id="family_po" name="family_po" class="float-right">
											
											  </li>
											   <li class="list-group-item">
												<b>Pin code</b>
												
													
													<input type="text"  id="family_pincode" name="family_pincode" class="float-right">
												
											  </li>
											  <li class="list-group-item">
											    <p style="color:red;display:none;" id="error_family_contact1">** Mobile Number should be 10 Character </p>
												<b>Contact Number 1</b> <input type="text" class="float-right " name="family_contact1" id="family_contact1" value="">
											  </li>
											  <li class="list-group-item">
											    <p style="color:red;display:none;" id="error_family_contact2">** Mobile Number should be 10 Character </p>
												<b>Contact Number 2</b> <input type="text" class="float-right " name="family_contact2" id="family_contact2" value="">
											  </li>
											
											  <li class="list-group-item">
												<b>Education Qualification</b> <input type="text" class="float-right " name="family_education" id="family_education" value="">
											  </li>
											   <li class="list-group-item">
												<b>Year of Educational Qualification
												</b> <select type="year" class="float-right " name="family_education_year" id="family_education_year"  style="border: 1px solid #8080804d;min-width: 180px;padding: 2px;">
												<option value="0000">Select Year</option>
												</select>
											  </li>
											  <li class="list-group-item">
												<b>Vocation</b> <input type="text" class="float-right " name="family_vocation" id="family_vocation" value="">
											  </li>
											  <li class="list-group-item family_dox">
												<b>Dox</b> <input type="file" class="float-right " id="family_dox" name="family_dox" >
												<span class="dox1" id="dox1">
													<select id="family_dox_type"  class="float-right" name="family_dox_type" >
															<option value=""   selected >Type of dox</option>
															<option value="IAFY1940">IAFY1940</option>
															<option value="PPO">PPO</option>
															<option value="Marksheet">Marksheet</option>
															<option value="Feereceipt">Feereceipt</option>
															<option value="Passbook">Passbook</option>
															<option value="Cancelled Cheque">Cancelled Cheque</option>
															<option value="Others">Others</option>
													</select>
													 <input type="text" class="float-right " id="family_dox_type_define" name="family_dox_type_define" placeholder="Type of dox">
													 <input type="text" class="float-right " id="family_dox_page_no" name="family_dox_page_no" placeholder="Page no">
													 <select type="year" class="float-right " name="family_dox_year" id="family_dox_year" >
													<option value="0000">Year of dox</option>
													</select>
												</span>
											  </li>
										  </ul>
										</form>
										  <div class="card-footer">
												  <div class="text-right">
												  
													<a href="#" id="add_family" class="btn btn-sm btn-primary" onclick="add_family()">
													  <i class="fas fa-user"></i> Save 
													</a>
													<a href="#" id="update_family" class="btn btn-sm btn-primary" onclick="update_family()">
													  Update
													</a>
										  </div>
										 
										  </div>
										   <br>
									</div>
									<div class="col-12 col-sm-12 col-md-12" style="display:contents;" id="table_data_family">
									 </div>
								</div>
							</div>
                  <!-- /.tab-pane -->
                </div>
        </div>
      </div>
    </div>
    </div>
           
            <!-- /.nav-tabs-custom -->
          </div>
          <!-- /.col -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
	<div id="myModal" class="modal">
     <div class="modal-content">
	 <div style="display:inline-flex;">
	 <H4>Add Document</H4>
	 
	 </div>
		<hr>
			<span >
			
			<input type="file"  id="dox_more" ><br>
			<label>Dox type</label>
			<select id="dox_type"  class="float-right" name="dox_type" >
															<option value=""   selected >Type of dox</option>
															<option value="IAFY1940">IAFY1940</option>
															<option value="PPO">PPO</option>
															<option value="Marksheet">Marksheet</option>
															<option value="Feereceipt">Feereceipt</option>
															<option value="Passbook">Passbook</option>
															<option value="Cancelled Cheque">Cancelled Cheque</option>
															<option value="Others">Others</option>
			</select><br>
			<span id="dox_type_define_1">
			<label>Define Dox type</label>
			<input type="text" class="float-right " id="dox_type_define" name="dox_type_define" placeholder="Type of dox"><br>
			</span>
			<label>Page Number</label>
			<input type="text" class="float-right " id="dox_page_no" name="dox_page_no" placeholder="Page no"><br>
			<label>Year of dox</label>
			<select type="year" class="float-right " name="dox_year" id="dox_year" >
													<option value="0000">Year of dox</option>
			</select><br>
			<hr>
			<input type="hidden" id="dta_id">
			<input type="hidden" id="dta_type">
			<input type="submit" class="btn btn-success" onclick="add_more_dox()">
			<button class="btn btn-danger" style="float:right;width: max-content;" onclick="close_model()">Close</button>
		</span>
		
		
    </div>

   </div>
      

  </div>
  <!-- /.content-wrapper -->
 <?php include('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->
<!-- jQuery -->

<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- AdminLTE App -->
<script src="dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->

<script src="address.js"></script>
<script src="address_family.js"></script>

<script src="dist/js/demo.js"></script>
<script src="plugins/select2/js/select2.full.min.js"></script>
<script src="plugins/sweetalert2/sweetalert2.min.js"></script>
<script src="plugins/toastr/toastr.min.js"></script>
<script>
function open_model(type,id){	
	$("#myModal").css('display','block');
	$("#dta_id").val(id);
	$("#dta_type").val(type);
		
}
function close_model(){
	$("#myModal").css('display','none');
	$("#dta_id").val('');
	$("#dta_type").val('');
	$("#dox_type_define").val('');
	$("#dox_page_no").val('');
	$('#dox_type_define_1').css('display','none');
	$("#dox_type option[value='']").prop("selected", true);
	$("#dox_year option[value='0000']").prop("selected", true);
	$("#dox_more").val(''); 
	
	
}

window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
}
</script>

	
	<!-----DATE PICKER----->
		<ul class="settings" style="display:none;">
				<li>
					<label><span id="selectedDate"></span>;</label>
				</li>
				<li>
					<label> <span id="selectedDateFormatted"></span>;</label>
				</li>
				<li>
					<label><span id="currentMonth"></span>;</label>
				</li>
    </ul>
	<link rel="stylesheet" href="dist/the-datepicker.css">
	<script src="dist/the-datepicker.js"></script>
	<script src="js/datePicker.js"></script>
	<!-----DATE PICKER----->
<script>
 const Toast = Swal.mixin({
					  toast: true,
					  position: 'top-end',
					  showConfirmButton: false,
					  timer: 3000
});
$(document).ready(function () {
	
	var dataString = 'type=sainik_rank';
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			//alert(data);
				//$('#rank').html(data);
				$('#rank_unit').html(data);
				$('#rank_others').html(data);
		}
	});
	
	var dataString = 'type=sambandh_unit_salutation';
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
				$('#Salutation_unit').html(data);
		}
	});
	
});
 $('#nok_dox').change(function(){
    var input = this;
	var url = $(this).val();
	if(url!=''){
		$('#dox').css('display','inline');
	}
	else{
		$('#dox').css('display','none');
	}
 });
$('#nok_dox_type').change(function(){
	 var input = this;
	  var val = $(this).val();
	 if(val=='Others'){
	 $('#nok_dox_type_define').css({'display':'inline','background': '#f3e5f52e'});
	}
	else{
		$('#nok_dox_type_define').css('display','none');
	}
 });
 $('#family_dox').change(function(){
    var input = this;
	var url = $(this).val();
	if(url!=''){
		$('#dox1').css('display','inline');
	}
	else{
		$('#dox1').css('display','none');
	}
 });
$('#family_dox_type').change(function(){
	 var input = this;
	  var val = $(this).val();
	 if(val=='Others'){
	 $('#family_dox_type_define').css({'display':'inline','background': '#f3e5f52e'});
	}
	else{
		$('#family_dox_type_define').css('display','none');
	}
 });
 $('#dox_type').change(function(){
	 var input = this;
	  var val = $(this).val();
	 if(val=='Others'){
	 $('#dox_type_define_1').css({'display':'inline','background': '#f3e5f52e'});
	}
	else{
		$('#dox_type_define_1').css('display','none');
	}
 });
 
 
function state(){
	var dataString = 'type=state';
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
		
		
		}
	});
}
function custom_pin_family(value){
	
	 if(value==101010){
		$("#family_pincode_custom").css('display','inline');
	   }else{
		   $("#family_pincode_custom").val('');
		   $("#family_pincode_custom").css('display','none');
	   }
}
function custom_pin_nok(value){
	
	 if(value==101010){
		$("#nok_pincode_custom").css('display','inline');
	   }else{
		   $("#nok_pincode_custom").css('display','none');
		   $("#nok_pincode_custom").val('');
	   }
}
function district_list(value,type,dist){
	
	   if(value=="Others"){
		   $("#"+type+"_state_custom").css('display','inline');
		   $("#"+type+"_district_custom").css('display','inline');
		   $("#"+type+"_pincode_custom").css('display','inline');
		   $("#"+type+"_tehsil_custom").css('display','inline');
		   $("#"+type+"_po_custom").css('display','inline');
		   
		    tehsil_list('Others',type);
			post_list('Others',type);
			pincode_list('Others',type);
			//if(type=='family'){custom_pin_family('101010');}else{custom_pin_nok('101010');}
			
	   }else{
		  
		   
		   $("#"+type+"_state_custom").val(''); 
		   $("#"+type+"_state_custom").css('display','none');
		   if($('#table_id_'+type).val()==''){
			   $("#"+type+"_district_custom").val(''); 
			   $("#"+type+"_pincode_custom").val(''); 
			   $("#"+type+"_tehsil_custom").val(''); 
			   $("#"+type+"_po_custom").val(''); 
			   $("#"+type+"_district_custom").css('display','none');
			   $("#"+type+"_pincode_custom").css('display','none');
			   $("#"+type+"_tehsil_custom").css('display','none');
			   $("#"+type+"_po_custom").css('display','none');
		   }
		    document.getElementById(type+"_tehsil").selectedIndex = "0";
		    document.getElementById(type+"_po").selectedIndex = "0";
		    document.getElementById(type+"_pincode").selectedIndex = "0";
		   
		 
		   
		   
	   }
		var dataString = 'type=district_list&state='+value+'&dist='+dist;
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){
					
					$('#'+type+'_district').html(data);
					return true;
			}
		});
	
}
function tehsil_list(value,type,thesil){
	 if(value=="Others"){
		   $("#"+type+"_district_custom").css('display','inline');
		   $("#"+type+"_pincode_custom").css('display','inline');
		   $("#"+type+"_tehsil_custom").css('display','inline');
		   $("#"+type+"_po_custom").css('display','inline');
		  
			post_list('Others',type);
			pincode_list('Others',type);
	   }
	   else{
		    
		    $("#"+type+"_district_custom").val(''); 
			 $("#"+type+"_district_custom").css('display','none');
			  if($('#table_id_'+type).val()==''){
			   $("#"+type+"_pincode_custom").val(''); 
			   $("#"+type+"_tehsil_custom").val(''); 
			   $("#"+type+"_po_custom").val('');
				$("#"+type+"_pincode_custom").css('display','none');
				$("#"+type+"_tehsil_custom").css('display','none');
				$("#"+type+"_po_custom").css('display','none');			   
			 }
		   	    
		    document.getElementById(type+"_po").selectedIndex = "0";
		    document.getElementById(type+"_pincode").selectedIndex = "0";

		   
		  
	   }
	var dataString = 'type=town_list&district='+value+'&thesil='+thesil;
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
				$('#'+type+'_tehsil').html(data);
				return true;
		}
	});
}
function post_list(value,type,post,dist){	
    if(value=="Others"){
		$("#"+type+"_tehsil_custom").css('display','inline');
		$("#"+type+"_pincode_custom").css('display','inline');
		$("#"+type+"_po_custom").css('display','inline');
		
			pincode_list('Others',type);
	   }
	    else{
		  
		  
		    $("#"+type+"_tehsil_custom").val(''); 
		    $("#"+type+"_tehsil_custom").css('display','none');
			
		     if($('#table_id_'+type).val()==''){
				 $("#"+type+"_pincode_custom").val(''); 
				 $("#"+type+"_po_custom").val(''); 
				 $("#"+type+"_pincode_custom").css('display','none');
				 $("#"+type+"_po_custom").css('display','none');
			}
		    document.getElementById(type+"_po").selectedIndex = "0";
		    document.getElementById(type+"_pincode").selectedIndex = "0";

		 
		  
	   }
	if(dist==null){
		var dist=$('#'+type+'_district').val();
	}	
	var dataString = 'type=post_list&town='+value+'&post='+post+'&dist='+dist;
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
				$('#'+type+'_po').html(data);
				return true;
		}
	});
}
function pincode_list(value,type,pin,dist){
	//alert(pin);
	 if(value=="Others"){
		$("#"+type+"_po_custom").css('display','inline');
		 $("#"+type+"_pincode_custom").css('display','inline');
		
	   }
	    else{
		  
		   $("#"+type+"_po_custom").val(''); 
		    document.getElementById(type+"_pincode").selectedIndex = "0";
		   $("#"+type+"_po_custom").css('display','none');
			
			 if($('#table_id_'+type).val()==''){
				   $("#"+type+"_pincode_custom").val(''); 
				   $("#"+type+"_pincode_custom").css('display','none');
			 }
	   }
	if(dist==null){
		var dist=$('#'+type+'_district').val();
	}	
	var dataString = 'type=pincode_list&post='+value+'&dist='+dist+'&pincode='+pin;
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
				$('#'+type+'_pincode').html(data);
				return true;
		}
	});
}
function state_list(state,type){
	var dataString = 'type=state_list&state='+state;
	$.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
				$('#'+type+'_state').html(data);
				if(type=='family'){
					$('#'+type+'_state1').html(data);
					return true;
				}
				
		}
	});
}
function myFunction(x) {
  if (x.matches) { // If media query matches
   $("#collapseOne").removeClass("show");
  // $("#collapseTwo").removeClass("show");
  } else {
    $("#collapseOne").addClass("show");
   // $("#collapseTwo").addClass("show");
  }
}


var x = window.matchMedia("(max-width: 450px)")
myFunction(x) // Call listener function at run time
x.addListener(myFunction);

$( document ).ready(function() {
	view_profile_parent();
	view_nok();
	view_family();
	view_unit();
	view_others();
	//state();
	//view_profile_account();
	//view_profie_unit();
	
});
function nok_relation_others(value){
	if(value=="OTHERS"){
		$("#nok_define_rel").css("display","inline");
		$("#nok_mrg_date").css("display","none");
	}
	else if(value=="WIFE"|| value=="HUSBAND"){
		$("#nok_mrg_date").css("display","inline");
		$("#nok_define_rel").css("display","none");
	}
	else{
		$("#nok_define_rel").css("display","none");
		$("#nok_mrg_date").css("display","none");
	}
}
function relation_others(value){
	 $('input[name="family_mar_status"]').prop("checked", false);
	  $("#family_dom").val('00. 00. 0000');
	if(value=="OTHERS"){
		$("#define_rel").css("display","inline");
		$("#mr_status").css("display","inline");
        
	}
	else if(value=="WIFE" || value=="HUSBAND"){
		$("#family_mrg_date").css("display","inline");
		$("#define_rel").css("display","none");
		$("#mr_status").css("display","none");
	}
	else if( value=="FATHER" || value=="MOTHER" || value==""){
		$("#family_mrg_date").css("display","none");
		$("#define_rel").css("display","none");
		$("#mr_status").css("display","none");
		$('input[name="family_mar_status"]').prop("checked", false);
	    $("#family_dom").val('00. 00. 0000');
	}
	else{
		$("#mr_status").css("display","inline");
		$("#define_rel").css("display","none");
		$("#family_mrg_date").css("display","none");
	}
}
$('input[name="family_mar_status"]').on('change', function(e) {
	
   var status=$('input[name="family_mar_status"]:checked').val();
 //  alert(status);
   if(status=='MARRIED'){
	   $("#family_mrg_date").css("display","inline");
   }
   else{
	  
	 $("#family_mrg_date").css("display","none");  
   }
});
function custom_sal_others(value){
	
	if(value=="Custom"){
		
		$(".cust").css("display","flex");	
	}
	else{
		$(".cust").css("display","none");	
	}
}
function custom_sal_family(value){	
	if(value=="Custom"){
		
		$("#cust_fam").css("display","inline");	
	}
	else{
		$("#cust_fam").css("display","none");	
	}
}
function custom_sal_nok(value){	
	if(value=="Custom"){
		
		$("#cust_nok").css("display","inline");	
	}
	else{
		$("#cust_nok").css("display","none");	
	}
}

function add_more_dox(){
	var id=$("#dta_id").val();
	var dta_type=$("#dta_type").val();
	var dox_type=$("#dox_type").val();
	var dox_type_define=$("#dox_type_define").val();
	var dox_page_no=$("#dox_page_no").val();
	var dox_year=$("#dox_year").val();
	var dataString = new FormData();
	var files = $("#dox_more")[0].files[0]; 
	dataString.append('id',id);
	dataString.append('files',files);
	dataString.append('dox_type',dox_type);
	dataString.append('dox_type_define',dox_type_define);
	dataString.append('dox_page_no',dox_page_no);
	dataString.append('dox_year',dox_year);
	if(dta_type=='nok'){
		dataString.append('type','add_more_nok_document');
	}else{
		dataString.append('type','add_more_family_document');
	}
	
	
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		contentType: false,
		processData: false,
		success: function(data){
				if(data==1){
					if(dta_type=='nok'){view_nok();}else{view_family();}
					$("#myModal").css('display','none');
					$('#dox_type_define_1').css('display','none');
	                $("#dta_id").val('');
	                $("#dta_type").val('');
	                $("#dox_type_define").val('');
	                $("#dox_page_no").val('');
					$("#dox_more").val('');
					$("#dox_type option[value='']").prop("selected", true);
					$("#dox_year option[value='0000']").prop("selected", true);	
					  Toast.fire({
						icon: 'success',
						title: 'New Document Successfully Updated'
					  });
				}else if(data=='dox_exits'){
					alert('This document already exits');
				}
				else if(data=='file_undefine'){alert('please add document');}			
		}
	});
	
}

function add_nok(){
	//alert($("form[name=nok_form_add]").serialize());
	// if($('#nok_Salutation').val()=='Custom'){
		// if($('#nok_custom_salutation').val()==''){
			// alert('Please Define Salutation');
			// return false;
		// }
	// }
	// if($('#nok_relation').val()=='Others'){
		// if($('#nok_define_others_rel').val()==''){
			// alert('Please Define Relation');
			// return false;
		// }
	// }
	// else if($('#nok_relation').val()=='Wife'){
		// if($('#nok_dom').val()==''){
			// alert('Please Choose married Date');
			// return false;
		// }
	// }
	if( $('#nok_contact1').val()!=''){
		if(mobile_validation('nok',$('#nok_contact1').val(),'error_nok_contact1')==false){
			return false;
		};
		
	}
	if( $('#nok_contact2').val()!=''){
		if(mobile_validation('nok',$('#nok_contact2').val(),'error_nok_contact2')==false){
			return false;
		};
		
	}
	
	var id=<?php echo $_GET['id']?>;
	var files = $('#nok_dox')[0].files[0]; 
	var nok_dox_type = $('#nok_dox_type').val(); 
	var nok_dox_page_no = $('#nok_dox_page_no').val(); 
	var nok_dox_year = $('#nok_dox_year').val(); 
	var nok_name = $('#nok_name').val(); 
	var nok_dox_type_define = $('#nok_dox_type_define').val(); 
	var dataString = new FormData();
	dataString.append('type','add_nok');
	dataString.append('id',id);
	dataString.append('files',files);
	if(typeof($('input[name=nok_gender]:checked').val())=='undefined'){
		dataString.append('nok_gender','');
	} 
	var x = $("form[name=nok_form_add]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
	if(confirm('Do you want to save the record?')==true){
			$.ajax({
				url:'backend/add_update_members.php',
				type: 'POST',
				data:dataString,
				contentType: false,
				processData: false,
				success: function(data){
				if(data==1){
					Toast.fire({
							icon: 'success',
							title: 'Successfully Added new record'
						  });
					 $("#show_nok").css('display','none');
					 $("#dox").css('display','none');
					 $("#nok_dox_type_define").css('display','none');
					 $("#add1").css('display','inline');
					 $("#table_data_nok").css('display','inline');
					 $("#min_nok").css('display','none');
					 $("#nok_dox_type_define").val('');
					 $("#nok_dox_page_no").val('');
					 $("#nok_note").val('');
					 $("#nok_dox").val('');
					 $(".custom_address").val('');
					 $(".custom_address").css('display','none');
					 $("#nok_dox_type option[value='']").prop("selected", true);
					 $("#nok_dox_year option[value='0000']").prop("selected", true);
					 view_nok(); 
				}	
						
				}
			});
	   
	}
}
function view_nok(){
	
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_nok&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			//alert(data);
				if(data!=''){
					$('#nok_add_status').css('display','none');
				}
				$('#table_data_nok').html(data);
				
		}
	});
}
function edit_nok(id){
	 $("#update_nok").css('display','inline');
	 $("#add_nok").css('display','none');
	 $("#show_nok").css('display','inline');
	 $("#add1").css('display','none');
	 //$("#min_nok").css('display','inline');
	 $(".nok_dox").css('display','none');
	 $('#table_data_nok').html('');
	 var dataString = 'type=edit_nok&id='+id;
	 $.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){
				
				var obj = jQuery.parseJSON(data);
				
				$('input[name=table_id_nok]').val(obj[0].id);
				$('input[name=nok_name]').val(obj[0].name);
				$('input[name=nok_gender][value='+obj[0].gender+']').attr('checked', true);
				$("#nok_Salutation option[value='"+obj[0].Salutation+"']").attr("selected","selected");
				$("#nok_relation option[value='"+obj[0].relation+"']").attr("selected","selected");
				if(obj[0].Salutation=='Custom'){
					 $("#cust_nok").css('display','inline');
					 $("#nok_custom_salutation").val(obj[0].custom_salutation);
			
				}
				if(obj[0].relation=='WIFE' || obj[0].relation=='HUSBAND'){
					 $("#nok_mrg_date").css('display','inline');
					 
					    var date1 = obj[0].dom;
						var newdate1 = date1.split("-").reverse().join("-");
						var newdate1 = newdate1.replace("-", ". ");
						var newdate1 = newdate1.replace("-", ". ");
						$("#nok_dom").val(newdate1);
				}
				else if(obj[0].relation=='OTHERS'){
					$("#nok_relation option[value='OTHERS']").attr("selected","selected");
					$("#nok_define_rel").css('display','inline');
					$("#nok_define_others_rel").val(obj[0].define_relation);
					
				}
				var date = obj[0].dob;
                var newdate = date.split("-").reverse().join("-");
                var newdate = newdate.replace("-", ". ");
                var newdate = newdate.replace("-", ". ");
				
				$('input[name=nok_dob]').val(newdate);
				$('input[name=nok_address1]').val(obj[0].address1);
				$('input[name=nok_address2]').val(obj[0].address2);
				$('input[name=nok_po]').val(obj[0].post);
				$('input[name=nok_tehsil]').val(obj[0].thesil);
				
				
				
				$('#nok_state').val(obj[0].state);
				$('#nok_district').val(obj[0].district);
				$('#nok_tehsil').val(obj[0].thesil);
				$('#nok_po').val(obj[0].post);
				$('#nok_pincode').val(obj[0].pincode);
				$("#nok_education_year OPTION[value='"+obj[0].education_year+"']").attr("selected","selected");
				// $("#nok_district option[value='"+obj[0].district+"']").attr("selected","selected");
				// $("#nok_town option[value='"+obj[0].town+"']").attr("selected","selected");
				if(obj[0].contact1==0){var contact1='';}else{ var contact1 =obj[0].contact1;}
				if(obj[0].contact2==0){var contact2='';}else{ var contact2 =obj[0].contact2;}
				$('input[name=nok_contact1]').val(contact1);
				$('input[name=nok_contact2]').val(contact2);
				$('input[name=nok_bank_name]').val(obj[0].bank_name);
				$('input[name=nok_bank_account]').val(obj[0].bank_account);
				$('input[name=nok_education]').val(obj[0].education);
				$('input[name=nok_vocation]').val(obj[0].vocation);
				var note=obj[0].note.split('-');
				//$('textarea[name=nok_note]').html(note[0]);
			}
		});
}
function update_nok(){
	// if($('#nok_relation').val()=='Others'){
		// if($('#nok_define_others_rel').val()==''){
			// alert('Please Define Relation');
			// return false;
		// }
	// }
	if( $('#nok_contact1').val()!=''){
		if(mobile_validation('nok',$('#nok_contact1').val(),'error_nok_contact1')==false){
			return false;
		};
		
	}
	if( $('#nok_contact2').val()!=''){
		if(mobile_validation('nok',$('#nok_contact2').val(),'error_nok_contact2')==false){
			return false;
		};
		
	}
    var id=<?php echo $_GET['id']?>;
	var dataString = new FormData();
	dataString.append('type','update_nok');
	dataString.append('id',id);
	if(typeof($('input[name=nok_gender]:checked').val())=='undefined'){
		
		dataString.append('nok_gender','');
	} 
	var x = $("form[name=nok_form_add]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
	if(confirm('Do you want to update the record?')==true){
	$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			contentType: false,
		    processData: false,
			success: function(message){
				
				if(message==1){
					Toast.fire({
						icon: 'success',
						title: 'Successfully updated record'
					  });
					
					 $('#table_id_nok').val('');
					 $("#nok_note").val('');
					 $('form[name=nok_form_add] input').val('');
					 $('form[name=nok_form_add] textarea').html('');
					 $(".custom_address").val('');
					 $(".custom_address").css('display','none');
					 $("#nok_define_rel").css('display','none');
					 $("#update_nok").css('display','none');
					 $("#add1").css('display','inline');
					 $("#min_nok").css('display','none');
					 $("#show_nok").css('display','none');
					 $("#add_nok").css('display','inline');
					 $(".nok_dox").css('display','inline');
					 view_nok();

				}
						
			}
		});	
	}
}
function delete_nok(id){
	var dataString = 'type=delete_nok&id='+id;
	if(confirm('Do you want to delete selected item?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
						Toast.fire({
						icon: 'success',
						title: 'Successfully deleted record'
					  });
						view_nok();
						
					
			}
		});
	}
	else{
		return false;
	}
}
function delete_nok_document(id,doc){
	
	var dataString = 'type=delete_nok_document&id='+id+'&doc='+doc;
	if(confirm('Do you want to delete selected item?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
			Toast.fire({
						icon: 'success',
						title: 'Successfully Deleted record'
					  });
						view_nok();
						
					
			}
		});
	}
	else{
		return false;
	}
}
function add_more_nok_document(id){
	var dataString = new FormData();
	var files = $("#mrnok"+id)[0].files[0]; 
	dataString.append('id',id);
	dataString.append('type','add_more_nok_document');
	dataString.append('files',files);
	
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		contentType: false,
		processData: false,
		success: function(data){	
		
				    $('#success_nok').val('Successfully updated record');
					$('.alert2').css('display','inline');
					view_nok();
					setTimeout(function(){ $('.alert2').css('display','none'); }, 2000);
				
		}
	});
	
}
function view_profile_parent(){
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_profile_parent&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#profile_parent_data').html(data);
				
				dd();
				
		}
	});
}
function edit_basic_profile(){
				
		$('.basci_profile_update').prop("disabled", false);
		$('.basic_profile').prop("disabled", false);
		$('.basic_profile').prop("readonly", false);
		$('.basic_profile').css("border", '1px solid #4527A0');
}
function update_basic_profile(){
	if(validation()==true){
		var id=<?php echo $_GET['id']?>;
		var data=$("form[name=update_parent_form]").serialize();
		var dataString = 'id='+id+'&type=update_member&'+data;
		//alert(dataString);
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(message){
				
				if(message==1){
					view_profile_parent();
					$('#success_parent').html('Successfully updated record');
					$('.success_parent').css('display','inline');
					$('.basci_profile_update').prop("disabled", true);
					$('.basic_profile').prop("readonly", true);
					$('.basic_profile').prop("disabled", true);
					$('.basic_profile').css("border", 'none');
					
					setTimeout(function(){ $('.alert_parent').css('display','none'); }, 2000);
				}
			}
		});
		
}
}
function delete_nok_notes(id,key){
	var dataString = 'type=delete_nok_notes&id='+id+'&key='+key;
	if(confirm('Do you want to delete selected item?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
						Toast.fire({
						icon: 'success',
						title: 'Successfully deleted record'
					  });
						view_nok();
						
					
			}
		});
	}
	else{
		return false;
	}
}
function edit_account_data(){
	
							 $('.account_data_update').prop("disabled", false);
							 $('.account_data').prop("readonly", false); 
							 $('.account_data').css("border", '1px solid #4527A0');
}
function update_account_data(){
		
			var dataString = 'id='+$('#account_id').val()+ '&nok_name='+ $('#nok_name').val() + '&nok_gps_pin='+ $('#nok_gps_pin').val()+'&nok_phone='+$('#nok_phone').val()+'&ppo='+$('#ppo').val()+'&pda='+$('#pda').val()+'&pension_bank='+$('#pension_bank').val()+'&pension_bank_branch='+$('#pension_bank_branch').val()+'&account_no='+$('#account_no').val()+'&type=update_member_account';
							$.ajax({
									url:'backend/add_update_members.php',
									type: 'POST',
									data:dataString,
									success: function(message){
										
										if(message==1){	
											 $('.success').css('display','inline');
											 $('#success').html('Successfully updated record');	
											 $('.account_data_update').prop("disabled", true);
											 $('.account_data').prop("readonly", true);
											 $('.account_data').css("border", 'none');
											 setTimeout(function(){ $('.alert1').css('display','none'); }, 2000);
										}
									}
							});
							
							
}
					   
function view_profie_unit(){
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_profie_unit&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#table_data_unit').html(data);
				
		}
	});
}

function view_profile_account(){
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_profile_account&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#table_data_account').html(data);
				
		}
	});
}
function add_family(){
	
	// if($('#family_Salutation').val()=='Custom'){
		// if($('#family_custom_salutation').val()==''){
			// alert('Please Define Salutation');
			// return false;
		// }
	// }
	// if($('#family_relation').val()=='Others'){
		// if($('#family_define_others_rel').val()==''){
			// alert('Please Define Relation');
			// return false;
		// }
	// }
	// else if($('#family_relation').val()=='Wife'){
		// if($('#family_dom').val()==''){
			// alert('Please Fill date of married');
			// return false;
		// }
	// }
	if( $('#family_contact1').val()!=''){
		if(mobile_validation('family',$('#family_contact1').val(),'error_family_contact1')==false){
			return false;
		};
		
	}
	if( $('#family_contact2').val()!=''){
		if(mobile_validation('family',$('#family_contact2').val(),'error_family_contact2')==false){
			return false;
		};
		
	}
	
		
	var id=<?php echo $_GET['id']?>;
	var files = $('#family_dox')[0].files[0]; 
	var dataString = new FormData();
	dataString.append('type','add_family');
	dataString.append('id',id);
	dataString.append('files',files);
	if(typeof($('input[name=family_gender]:checked').val())=='undefined'){
		dataString.append('family_gender','');
	} 
	if($('#family_relation').val()=='WIFE' || $('#family_relation').val()=='MOTHER' || $('#family_relation').val()=='FATHER' || $('#family_relation').val()=='HUSBAND'){
		
		dataString.append('family_mar_status','MARRIED');
	}
	else if($('#family_relation').val()==''){
		dataString.append('family_mar_status','');
	}
	var x = $("form[name=family_form_add]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
   if(confirm('Do you want to save the record?')==true){
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		contentType: false,
		processData: false,
		success: function(data){
       if(data==1){
		   Toast.fire({
						icon: 'success',
						title: 'Successfully Added new record'
					  });
		 $("form[name=family_form_add] input").val('');
		 
		 $("#show_fam").css('display','none');
		 $("#dox1").css('display','none');
		 $("#family_dox_type_define").css('display','none');
		 $("#add_fam").css('display','inline');
		 $("#min_fam").css('display','none');
		 $("#table_data_family").css('display','inline');
		 $("#family_dox").val('');
		 $("#family_dox_type_define").val('');
		$("#family_dox_page_no").val('');
		 $(".custom_address").val('');
		$(".custom_address").css('display','none');
		$("#family_dox_type option[value='']").prop("selected", true);
		$("#family_dox_year option[value='0000']").prop("selected", true);
		$('input[name=family_gender]').attr('checked', false);
	     view_family();
	   }
          
				
		}
	});
   }
}
function delete_family(id){
	var dataString = 'type=delete_family&id='+id;
	if(confirm('Do you want to delete selected item?')==true){
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				   Toast.fire({
						icon: 'success',
						title: 'Successfully Deleted record'
					  });
					view_family();
					
				
		}
	});
	}
	else{
		return false;
	}
}
function edit_family(id){
	 $("#update_family").css('display','inline');
	 $("#add_family").css('display','none');
	 $("#show_fam").css('display','inline');
	 $("#add_fam").css('display','none');
	 $(".family_dox").css('display','none');
	 $('#table_data_family').html('');
	 var dataString = 'type=edit_family&id='+id;
	 $.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){
				
				var obj = jQuery.parseJSON(data);
				
				$('input[name=table_id_family]').val(obj[0].id);
				$('input[name=family_name]').val(obj[0].name);
				$('input[name=family_gender][value='+obj[0].gender+']').attr('checked', true);
				//$('select[name=nok_relation]').val(obj[0].relation);
				// $("#family_r_"+).prop("selected", true);
				// $("#family_saf_"+obj[0].Salutation).prop("selected", true);
				$("#family_Salutation option[value='"+obj[0].Salutation+"']").prop("selected", true);
				$("#family_relation option[value='"+obj[0].relation+"']").prop("selected", true);
				if(obj[0].Salutation=='Custom'){
					 $("#cust_fam").css('display','inline');
					 $("#family_custom_salutation").val(obj[0].custom_salutation);
			
				}
				if(obj[0].relation=='WIFE' || obj[0].relation=='HUSBAND'){
					 $("#family_mrg_date").css('display','inline');
					 var date1 = obj[0].dom;
					 var newdate1 = date1.split("-").reverse().join("-");
					 var newdate1 = newdate1.replace("-", ". ");
					 var newdate1 = newdate1.replace("-", ". ");
					 $("#family_dom").val(newdate1);
			
				}
				else if(obj[0].relation=='MOTHER' || obj[0].relation=='FATHER' || obj[0].relation==''){
					 $("#family_mrg_date").css('display','none');
					 
			
				}
				else if(obj[0].relation=='OTHERS'){
					
					$("#family_relation option[value='OTHERS']").attr("selected","selected");
					$("#define_rel").css('display','inline');
					$("#mr_status").css('display','inline');
					
					 $("#family_define_others_rel").val(obj[0].define_relation);
					if(obj[0].mr_status=='MARRIED'){
						$("#family_mrg_date").css('display','inline');
						
						 $('input[name="family_mar_status"]:nth(0)').prop("checked", true);
						 var date1 = obj[0].dom;
						 var newdate1 = date1.split("-").reverse().join("-");
						 var newdate1 = newdate1.replace("-", ". ");
						 var newdate1 = newdate1.replace("-", ". ");
						 $("#family_dom").val(newdate1);
						 
					}
					else{
						 $('input[name="family_mar_status"]:nth(1)').prop("checked", true);
						 $("#family_mrg_date").css('display','none');
					}
				}
				else{
					if(obj[0].mr_status=='MARRIED'){
						$("#family_mrg_date").css('display','inline');
						$("#mr_status").css('display','inline');
						 $('input[name="family_mar_status"]:nth(0)').prop("checked", true);
						 var date1 = obj[0].dom;
						 var newdate1 = date1.split("-").reverse().join("-");
						 var newdate1 = newdate1.replace("-", ". ");
						 var newdate1 = newdate1.replace("-", ". ");
						 $("#family_dom").val(newdate1);
						 
					}
					else{
						$("#family_mrg_date").css('display','none');
						$("#mr_status").css('display','inline');
						 $('input[name="family_mar_status"]:nth(1)').prop("checked", true);
					}
				}
				var date = obj[0].dob;
                var newdate = date.split("-").reverse().join("-");
                var newdate = newdate.replace("-", ". ");
                var newdate = newdate.replace("-", ". ");
				
				$('#family_dob').val(newdate);
				//$('input[name=family_dob]').val(obj[0].dob);
				$('input[name=family_address1]').val(obj[0].address1);
				$('input[name=family_address2]').val(obj[0].address2);
				
				
				$('#family_state').val(obj[0].state);
				$('#family_district').val(obj[0].district);
				$('#family_tehsil').val(obj[0].thesil);
				$('#family_po').val(obj[0].post);
				$('#family_pincode').val(obj[0].pincode);
				
			    $("#family_education_year option[value='"+obj[0].education_year+"']").prop("selected", true);
				if(obj[0].contact1==0){var contact1='';}else{ var contact1 =obj[0].contact1;}
				if(obj[0].contact2==0){var contact2='';}else{ var contact2 =obj[0].contact2;}
				$('input[name=family_contact1]').val(contact1);
				$('input[name=family_contact2]').val(contact2);
				$('input[name=family_education]').val(obj[0].education);
				$('input[name=family_vocation]').val(obj[0].vocation);
			}
		});
}
function update_family(){
	// if($('#family_relation').val()=='Others'){
		// if($('#family_define_others_rel').val()==''){
			// alert('Please Define Relation');
			// return false;
		// }
	// }
	
	if( $('#family_contact1').val()!=''){
		if(mobile_validation('family',$('#family_contact1').val(),'error_family_contact1')==false){
			return false;
		};
		
	}
	if( $('#family_contact2').val()!=''){
		if(mobile_validation('family',$('#family_contact2').val(),'error_family_contact2')==false){
			return false;
		};
		
	}
    var id=<?php echo $_GET['id']?>;
	var dataString = new FormData();
	dataString.append('type','update_family');
	dataString.append('id',id);
	if(typeof($('input[name=family_gender]:checked').val())=='undefined'){
		
		dataString.append('family_gender','');
	} 
	if($('#family_relation').val()=='WIFE' || $('#family_relation').val()=='MOTHER' || $('#family_relation').val()=='FATHER' || $('#family_relation').val()=='HUSBAND'){
		
		dataString.append('family_mar_status','MARRIED');
	}
	else if($('#family_relation').val()=='' ){
		dataString.append('family_mar_status','');
	}
	var x = $("form[name=family_form_add]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
	 
    });
	
	//dataString.append('family_state',$('#family_state').val());
	if(confirm('Do you want to update the record?')==true){
	$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			contentType: false,
		    processData: false,
			success: function(message){
				
				if(message==1){
				      Toast.fire({
						icon: 'success',
						title: 'Successfully updated record'
					  });
					  $('#table_id_family').val('');
					  $("#family_state1 option[value='']").html("");
				      $("#family_district").html('');
					  $("#family_po").html('');
					  $("#family_pincode").html('');
					  $("#family_tehsil").html('');
					 // state();
					// $('input[type=radio]').prop('checked', false);
					 $('input[name=family_gender]').attr('checked', false);
					 $('input[name="family_mar_status"]').prop("checked", false);
	                 $("#family_dom").val('00. 00. 0000');
                     $("#family_Salutation option[value='']").prop("selected", true);
				     $("#family_relation option[value='']").prop("selected", true);
				     $("#family_education_year option[value='0000']").prop("selected", true);
				     $("#family_mrg_date").css("display","none");
		             $("#mr_status").css("display","none");
					 $('input[type=text]').val('');
					  $(".custom_address").val('');
					 $(".custom_address").css('display','none');
					 $("#update_family").css('display','none');
					 $("#define_rel").css('display','none');
					 $("#add_fam").css('display','inline');
					 $("#show_fam").css('display','none');
					 $("#add_family").css('display','inline');
					 $("#min_fam").css('display','none');
					 $(".family_dox").css('display','inline');
					 view_family();
				}
						
			}
		});	
	}
}
function view_family(){
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_family&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				$('#table_data_family').html(data);
				
		}
	});
}
function add_more_family_document(id){
	var dataString = new FormData();
	var files = $("#mrfam"+id)[0].files[0]; 
	dataString.append('id',id);
	dataString.append('type','add_more_family_document');
	dataString.append('files',files);
	
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		contentType: false,
		processData: false,
		success: function(data){	
		
				    $('#success2').val('Successfully updated record');
					$('.alert2').css('display','inline');
					view_family();
					setTimeout(function(){ $('.alert2').css('display','none'); }, 2000);
				
		}
	});
	
}
function delete_family_document(id,doc){
	
	var dataString = 'type=delete_family_document&id='+id+'&doc='+doc;
	if(confirm('Do you want to delete selected item?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
			//alert(data);
						Toast.fire({
						icon: 'success',
						title: 'Successfully Deleted record'
					    });
						view_family();
						
					
			}
		});
	}
	else{
		return false;
	}
}
function validation(){
	
	
	if($('#army_no').val()=='' || $('#name').val()==''  || $('#cas_dt').val() =='' || $('#rank').val()=='' ){
		if($('#army_no').val()==''){$('#army_no').css('border','1px solid red')}
		if($('#name').val()==''){$('#name').css('border','1px solid red')}
		//if($('#serving_unit').val()==''){$('#serving_unit').css('border','1px solid red')}
		if($('#cas_dt').val()==''){$('#cas_dt').css('border','1px solid red')}
		//if($('#parent_unit').val()==''){$('#parent_unit').css('border','1px solid red')}
		if($('#rank').val()==''){$('#rank').css('border','1px solid red')}
		$('#error').html('** Please Fill mandatory  field');
		$('#error').css('display','inline');
		return false;
	}
	
	else{
		$('#error').css('display','none');
		$('input').css('border','1px solid #8080805e');
		return true;
	}
}
function add_unit(){
	if($('#contact1_unit').val()!=''){
		
		if(mobile_validation('unit',$('#contact1_unit').val(),'error_unit_contact1')==false){
			return false;
		};
		
	}
	if( $('#contact2_unit').val()!=''){
		if(mobile_validation('unit',$('#contact2_unit').val(),'error_unit_contact2')==false){
			return false;
		};
		
	}
	if( $('#email_unit').val()!=''){
		if(email_validation('unit',$('#email_unit').val(),'error_unit_email')==false){
			return false;
		};
		
	}
//if(validation_unit()==true){
	var id=<?php echo $_GET['id']?>;
	var dataString = new FormData();
	dataString.append('type','add_unit');
	dataString.append('id',id);
	var x = $("form[name=add_unit_form]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
	if(confirm('Do you want to save the record?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			contentType: false,
		    processData: false,  
			success: function(message){

				if(message==1){	
					Toast.fire({
						icon: 'success',
						title: 'Successfully Added new record'
					  });
					view_unit();
					$("form[name=add_unit_form] input").val('');
					$("form[name=add_unit_form] textarea").val('');
					$("#show_unit_form").css('display','none');
					$("#max_unit").css('display','inline');
					$("#min_unit").css('display','none');
					
					
				}
			}
		});
	}
//}	
}	
function view_unit(){
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_unit&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			
				$('#table_data_unit').html(data);
				
		}
	});
}
function edit_unit(id){
	 $("#show_unit_form").css('display','inline');
	 $("#max_unit").css('display','none');
	 	
	 $("#add_unit_but").css('display','none');	
	 $("#update_unit_but").css('display','inline');	
	 
	 var dataString = 'type=edit_unit&id='+id;
	 $.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
				var obj = jQuery.parseJSON(data);	
				$('input[name=table_id_unit]').val(obj[0].id);
				$('input[name=name_unit]').val(obj[0].name);
				$("#unit_r_"+obj[0].rank).prop("selected", true);
				$("#Salutation_unit option[value='"+obj[0].Salutation+"']").attr("selected","selected");
				$("#rank_unit option[value='"+obj[0].rank+"']").attr("selected","selected");
				if(obj[0].contact1==0){var contact1='';}else{ var contact1 =obj[0].contact1;}
				if(obj[0].contact2==0){var contact2='';}else{ var contact2 =obj[0].contact2;}
				$('input[name=contact1_unit]').val(contact1);
				$('input[name=contact2_unit]').val(contact2);
				$('input[name=email_unit]').val(obj[0].email);
				$('input[name=appointment_unit]').val(obj[0].appointment);
				$('input[name=org_unit]').val(obj[0].unit_org);
				$('textarea[name=remarks_unit]').html(obj[0].remarks);
				
			}
		});
}
function update_unit(){	
    if($('#contact1_unit').val()!=''){
		
		if(mobile_validation('unit',$('#contact1_unit').val(),'error_unit_contact1')==false){
			return false;
		};
		
	}
	if( $('#contact2_unit').val()!=''){
		if(mobile_validation('unit',$('#contact2_unit').val(),'error_unit_contact2')==false){
			return false;
		};
		
	}
	if( $('#email_unit').val()!=''){
		if(email_validation('unit',$('#email_unit').val(),'error_unit_email')==false){
			return false;
		};
		
	}
//if(validation_unit()==true){
	var id=<?php echo $_GET['id']?>;
	var dataString = new FormData();
	dataString.append('type','update_unit');
	dataString.append('id',id);
	var x = $("form[name=add_unit_form]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
	if(confirm('Do you want to update the record?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			contentType: false,
		    processData: false,
			success: function(message){

				if(message==1){
				 view_unit();
				 $("#show_unit_form").css('display','none');
				 $("#max_unit").css('display','inline');
				
				 $("#add_unit_but").css('display','inline');	
				 $("#update_unit_but").css('display','none');
				 $("form[name=add_unit_form] input").val('');
				 $("form[name=add_unit_form] textarea").html('');
				 $('#success2').val('Successfully updated record');
				 Toast.fire({
						icon: 'success',
						title: 'Successfully updated record'
					  });
				}
			}
		});
	}
							
//}
}
function delete_unit(id){
	
	var dataString = 'type=delete_unit&id='+id;
	if(confirm('Do you want to delete selected item?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
					Toast.fire({
						icon: 'success',
						title: 'Successfully deleted record'
					  });
						view_unit();
					
			}
		});
	}
	else{
		return false;
	}
}
function validation_unit(){
	if($('#rank_unit').val()=='' || $('#name_unit').val()=='' || $('#contact1_unit').val()=='' || $('#appointment_unit').val()==''  ){
		if($('#rank_unit').val()==''){$('#rank_unit').css('border','1px solid red')}
		if($('#name_unit').val()==''){$('#name_unit').css('border','1px solid red')}
		if($('#contact1_unit').val()==''){$('#contact1_unit').css('border','1px solid red')}
		if($('#appointment_unit').val()==''){$('#appointment_unit').css('border','1px solid red')}
		
		$('#error_unit').html('** Please Fill mandatory  field');
		$('#error_unit').css('display','inline');
		return false;
	}
	else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#email_unit').val()))){
		$('#email_unit').css('border','1px solid red');
		$('#error_unit').html('** Given email wrong format');
		$('#error_unit').css('display','inline');
		return false;
	}
	else if($('#contact1_unit').val().length >10 || $('#contact1_unit').val().length < 10){
		$('input').css('border','1px solid #8080805e');
		$('#contact1_unit').css('border','1px solid red');
		$('#error_unit').html('** Mobile Number should be 10 charactor');
		$('#error_unit').css('display','inline');
		return false;
	}
	else{
		$('#error_unit').css('display','none');
		$('input').css('border','1px solid #8080805e');
		return true;
	}
	// if($('#email_unit').val()!=''){
		// if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#email').val()))){
			// $('#email_unit').css('border','1px solid red');
			// $('#error_unit').html('** Given email wrong format');
			// $('#error_unit').css('display','inline');
			// return false;
		// }
	// }
	
}
function add_others(){
//if(validation_others()==true){
	// if($('#Salutation_others').val()=='Custom'){
		// if($('#custom_salutation_others').val()==''){
			// alert('Please Define Salutation');
			// return false;
		// }
	// }
	if( $('#email_others').val()!=''){
		if(email_validation('unit',$('#email_others').val(),'error_others_email')==false){
			return false;
		};
		
	}
	if( $('#contact1_others').val()!=''){
		if(mobile_validation('others',$('#contact1_others').val(),'error_others_contact1')==false){
			return false;
		};
		
	}
	if( $('#contact2_others').val()!=''){
		if(mobile_validation('others',$('#contact2_others').val(),'error_others_contact2')==false){
			return false;
		};
		
	}
	var id=<?php echo $_GET['id']?>;
	var dataString = new FormData();
	dataString.append('type','add_others');
	dataString.append('id',id);
	var x = $("form[name=add_others_form]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
		if(confirm('Do you want to save the record?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			contentType: false,
		    processData: false,
			success: function(message){

				if(message==1){	
					Toast.fire({
						icon: 'success',
						title: 'Successfully Added new record'
					  });
					view_others();
					$("form[name=add_others_form] input").val('');
					$("form[name=add_others_form] textarea").val('');
					$("#show_others_form").css('display','none');
					$("#max_others").css('display','inline');
					$("#min_others").css('display','none');
				
				}
			}
		});
		}
//}	
}	
function view_others(){
	var id=<?php echo $_GET['id']?>;
	var dataString = 'type=view_others&parent_id='+id;
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){
			
				$('#table_data_others').html(data);
				
		}
	});
}
function edit_others(id){
	$("#show_others_form").css('display','inline');
	 $("#max_others").css('display','none');
	 //$("#min_others").css('display','inline');	
	 $("#add_others_but").css('display','none');	
	 $("#update_others_but").css('display','inline');	
	 var dataString = 'type=edit_others&id='+id;
	 $.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			success: function(data){	
				var obj = jQuery.parseJSON(data);	
				if(obj[0].Salutation=='Custom'){
					 $(".cust").css('display','flex');
					 $("#custom_salutation_others").val(obj[0].custom_salutation);
				}
				$('input[name=table_id_others]').val(obj[0].id);
				$('input[name=name_others]').val(obj[0].name);
				$("#Salutation_others option[value='"+obj[0].Salutation+"']").attr("selected","selected");
				$("#rank_others option[value='"+obj[0].rank+"']").attr("selected","selected");
				if(obj[0].contact1==0){var contact1='';}else{ var contact1 =obj[0].contact1;}
				if(obj[0].contact2==0){var contact2='';}else{ var contact2 =obj[0].contact2;}
				$('input[name=contact1_others]').val(contact1);
				$('input[name=contact2_others]').val(contact2);
				$('input[name=email_others]').val(obj[0].email);
				$('input[name=appointment_others]').val(obj[0].appointment);
				$('input[name=org_others]').val(obj[0].unit_org);
				$('textarea[name=remarks_others]').html(obj[0].remarks);
				
			}
		});
}
function update_others(){	
    if( $('#email_others').val()!=''){
		if(email_validation('unit',$('#email_others').val(),'error_others_email')==false){
			return false;
		};
		
	}
   if( $('#contact1_others').val()!=''){
		if(mobile_validation('others',$('#contact1_others').val(),'error_others_contact1')==false){
			return false;
		};
		
	}
	if( $('#contact2_others').val()!=''){
		if(mobile_validation('others',$('#contact2_others').val(),'error_others_contact2')==false){
			return false;
		};
		
	}
//if(validation_others()==true){
	var id=<?php echo $_GET['id']?>;
	var dataString = new FormData();
	dataString.append('type','update_others');
	dataString.append('id',id);
	var x = $("form[name=add_others_form]").serializeArray();
     $.each(x, function(i, field){
      dataString.append(field.name,field.value);
    });
	if(confirm('Do you want to update the record?')==true){
		$.ajax({
			url:'backend/add_update_members.php',
			type: 'POST',
			data:dataString,
			contentType: false,
		    processData: false,
			success: function(message){

				if(message==1){
					view_others();
				 $("#show_others_form").css('display','none');
				 $("#max_others").css('display','inline');
				 //$("#min_others").css('display','none');	
				 $("#add_others_but").css('display','inline');	
				 $("#update_others_but").css('display','none');
				 $("form[name=add_others_form] input").val('');
				 $("form[name=add_others_form] textarea").html('');
			     	Toast.fire({
						icon: 'success',
						title: 'Successfully updated record'
					  });
				}
			}
		});
	}
							
//}
}
function delete_others(id){
	
	var dataString = 'type=delete_others&id='+id;
	if(confirm('Do you want to delete selected item?')==true){
    $.ajax({
		url:'backend/add_update_members.php',
		type: 'POST',
        data:dataString,
		success: function(data){	
				Toast.fire({
						icon: 'success',
						title: 'Successfully Deleted record'
					  });
					view_others();
				
		}
	});
	}
	else{
		return false;
	}
}
function email_validation(form_type,email,id){
	if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(email))){
		$('#'+id).css('display','block');
		return false;
	}
	else{
		$("#"+id).css('display','none');
		return true;
	}
}
function mobile_validation(form_type,mobile_no,id){
	if(mobile_no.length >10 || mobile_no.length < 10){
		$('#'+id).css('display','block');
		return false;
	}
	else{
		$("#"+id).css('display','none');
		return true;
	}
}
function validation_others(){
	if($('#rank_others').val()=='' || $('#name_others').val()=='' || $('#contact1_others').val()=='' || $('#appointment_others').val()==''  ){
		if($('#rank_others').val()==''){$('#rank_others').css('border','1px solid red')}
		if($('#name_others').val()==''){$('#name_others').css('border','1px solid red')}
		if($('#contact1_others').val()==''){$('#contact1_others').css('border','1px solid red')}
		if($('#appointment_others').val()==''){$('#appointment_others').css('border','1px solid red')}
		
		$('#error_others').html('** Please Fill mandatory  field');
		$('#error_others').css('display','inline');
		return false;
	}
	else if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#email_others').val()))){
		$('#email_others').css('border','1px solid red');
		$('#error_others').html('** Given email wrong format');
		$('#error_others').css('display','inline');
		return false;
	}
	else if($('#contact1_others').val().length >10 || $('#contact1_others').val().length < 10){
		$('input').css('border','1px solid #8080805e');
		$('#contact1_others').css('border','1px solid red');
		$('#error_others').html('** Mobile Number should be 10 charactor');
		$('#error_others').css('display','inline');
		return false;
	}
	else{
		$('#error_others').css('display','none');
		$('input').css('border','1px solid #8080805e');
		return true;
	}
	// if($('#email_others').val()!=''){
		// if(!(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test($('#email').val()))){
			// $('#email_others').css('border','1px solid red');
			// $('#error_others').html('** Given email wrong format');
			// $('#error_others').css('display','inline');
			// return false;
		// }
	// }
	
}


$(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script>
			function max_unit(){

				 $("#show_unit_form").css('display','inline');
				 $("#max_unit").css('display','none');
				 $("#min_unit").css('display','inline');	 
			}
			function min_unit(){
				 $("#show_unit_form").css('display','none');
				 $("#max_unit").css('display','inline');
				 $("#min_unit").css('display','none');	 
			}
			function max_others(){
				 $("#show_others_form").css('display','inline');
				 $("#max_others").css('display','none');
				 $("#min_others").css('display','inline');	 
			}
			function min_others(){
				 $("#show_others_form").css('display','none');
				 $("#max_others").css('display','inline');
				 $("#min_others").css('display','none');	 
			}
			function add(){
				$("#table_data_family").css('display','none');
				$("#update_family").css('display','none');
				 $("#add_family").css('display','inline');
				 $("#show_fam").css('display','inline');
				 $("#add_fam").css('display','none');
				 $("#min_fam").css('display','inline');	 
			}
			function rme(){
				$("#table_data_family").css('display','inline');
				 $("#show_fam").css('display','none');
				 $("#add_fam").css('display','inline');
				 $("#min_fam").css('display','none');	 
			}
			function add1(){
				$("#nok_dox").css('display','inline');
				$("#update_nok").css('display','none');
				 $("#add1").css('display','inline');
				 $("#show_nok").css('display','inline');
				 $("#add1").css('display','none');
				 $("#min_nok").css('display','inline');	 
			}
			function rme1(){
				
				 $("#show_nok").css('display','none');
				 $("#add1").css('display','inline');
				 $("#min_nok").css('display','none');	 
			}

			</script>
			<script>
			  window.onload = function () {
        //Reference the DropDownList.
        var ddlYears = document.getElementById("nok_education_year");
       
        var ddlYears1 = document.getElementById("family_education_year");
		 var ddlYears2 = document.getElementById("nok_dox_year");
		  var ddlYears3 = document.getElementById("family_dox_year");
		  var ddlYears4 = document.getElementById("dox_year");
        //Determine the Current Year.
        var currentYear = (new Date()).getFullYear();
 
        //Loop and add the Year values to DropDownList.
        for (var i = currentYear; i >= 1950; i--) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
            ddlYears.appendChild(option);
			 
        }
		 for (var i = currentYear; i >= 1950; i--) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
           
			  ddlYears1.appendChild(option);
        }
		 for (var i = currentYear; i >= 1950; i--) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
           
			  ddlYears2.appendChild(option);
        }
		 for (var i = currentYear; i >= 1950; i--) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
           
			  ddlYears3.appendChild(option);
        }
		 for (var i = currentYear; i >= 1950; i--) {
            var option = document.createElement("OPTION");
            option.innerHTML = i;
            option.value = i;
           
			  ddlYears4.appendChild(option);
        }
		
    };
			</script>
			

<!-- my date-selector script -->
<script src="./src/gm-date-selector.js"></script>

<!-- my date-selector config -->
<script type="text/javascript">
    dateSelector( '.dateInput');
</script>



<!-- The Modal -->

</body>
</html>
