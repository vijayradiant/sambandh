<?php 
session_start();
if(!isset($_SESSION['name'])){
		header('location:login.php');
}
?>
<!DOCTYPE html>
<html>
<?php INCLUDE('head.php');?>
<body class="hold-transition sidebar-mini">
<div class="wrapper">
  <!-- Navbar -->
<?php include('nav.php'); ?>
  <!-- /.navbar -->

  <!-- Main Sidebar Container -->
  <?PHP INCLUDE('sidebar.php'); ?>
<style>
.select2-container .select2-selection--single {
    box-sizing: border-box;
    cursor: pointer;
    display: block;
    height: 37px;
    user-select: none;
    -webkit-user-select: none;
	border: 1px solid #8080805e;
}
#error{
	color:red;
	display:none;
}
#loading{
	display:none;
}
</style>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Add new Sainik</li>
            </ol>
          </div>
		   <div class="alert success" style="display:none;">
			  <span class="closebtn">&times;</span>  
			  <strong>Success!</strong> <span id="success"></span>
			</div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <div class="row">
          <!-- left column -->
          <div class="col-md-12">
            <!-- general form elements -->
            <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Update Bulk data</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
             
                <div class="card-body">
				  <form action="" name="parent_form">
				 
					<div class="row">
					<div class="col-md-3">
					<div class="col-md-12">
						<div class="form-group">
							<label for="">Type</label>
							<select id="data_type" name="data_type" class="form-control select2 select2-danger" data-dropdown-css-class="select2-danger" style="width: 100%;">
								<option value="sainik">Sainik</option>
								<option value="Nok">Nok</option>
								<!--<option value="Family">Family</option>
								<option value="Unit">Unit</option>
								<option value="Others">Others</option>!-->
							</select>
						</div>
					</div>
					
					<div class="col-md-12">
						<div class="form-group">
							<label for=""> Excel</label><span style="color:#0288D1;font-size:12px;">** Excel must be in CSV Formate</span>
							<input type="file" id="files" name="files" class="form-control"  placeholder="Enter Name">
							
						</div>
						<div>
						<p id="error">Data's not added fully.please refer this Excel <a id="error_link" download>Download error</a></p>
						</div>
						<img src="load.gif" id="loading" width="85px">
					</div>
					</div>
					<div class="col-md-1"></div>
					<div class="col-md-8">
					<a href="pointFile/docsMove.xls"  alt="sampleSheet" style="text-decoration: none;color:black" >Sample Download</a>
					<div style="    border: 1px solid #0000002b;border-radius: 3px;padding: 5px;">
					<a href="excel_sample/sainik.csv"  alt="sampleSheet" >1) Sainik.csv</a><br>
					<a href="excel_sample/nok.csv"  alt="sampleSheet" >2) Nok.csv</a><br>
					<!--<a href="excel_sample/family.csv"  alt="sampleSheet" >3) Add Family</a><br>
					<a href="excel_sample/unit.csv"  alt="sampleSheet" >4) Add Unit</a><br>
					<a href="excel_sample/others.csv"  alt="sampleSheet" >5) Add Others</a><br>!-->
					<label>NOTE : </label><br>
					<span>1) Add Sainik - Armyno(pers no) should be unique one </span><br>
					<span>2) Armyno,Sainik name,Cas date fields are mandatory</span>
					<!--<label>Step 1 : </label>
					<span>Add Sainik details first.Here Armyno(pers no) should be unique one</span><br>
					<label>Step 2 : </label>
					<span>After completing sainik data,then proceed to add NOK,Unit and Others details.</span>!-->
					
					</div>
					
					</div>
					</div>
					 <div class="row">
				    
				  </div>
				  
				   </form>
				 
				  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" id="add" name="add" onclick="add_data()" class="btn btn-primary">Submit</button>
				
                </div>
            
            </div>
            <!-- /.card -->

              
          </div>
          <!--/.col (right) -->
        </div>
        <!-- /.row -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
<?php include('footer.php'); ?>

  <!-- Control Sidebar -->
  <aside class="control-sidebar control-sidebar-dark">
    <!-- Control sidebar content goes here -->
  </aside>
  <!-- /.control-sidebar -->
</div>
<!-- ./wrapper -->

<!-- jQuery -->
<script src="plugins/jquery/jquery.min.js"></script>
<!-- Bootstrap 4 -->
<script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- bs-custom-file-input -->
<script src="plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="plugins/select2/js/select2.full.min.js"></script>

<script src="dist/js/adminlte.min.js"></script>

<script src="dist/js/demo.js"></script>
<script>


function add_data(){	
	
	
	var dataString = new FormData();
	dataString.append('type',$("#data_type").val());
	//dataString.append('type','bulk');
	dataString.append('files',$("#files")[0].files[0]);
	$.ajax({
		url:'backend/excel_update_backend.php',
		type: 'POST',
        data:dataString,
		contentType: false,
		processData: false,
		beforeSend: function(){
			 $("#loading").show();
		},
		complete: function(){
			 $("#loading").hide();
		},
		success: function(message){	
			if(message==1){
				$("#error").css('display','none');
				$('input[type=file]').val('');
				$('#success').val('Successfully Added');
				$('.alert').css('display','inline');
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
			else if(message=='Family'){

				alert('Dont have Family members details in this excel sheet');
			}
			else{
				$("#error").css('display','inline');
				$("#error_link").attr('href','backend/excel_error_document/'+message);
			}
			
		}
	});
}
function add_data1(){	
	
	
	var dataString = new FormData();
	//dataString.append('type',$("#data_type").val());
	dataString.append('type','bulk');
	dataString.append('files',$("#files")[0].files[0]);
	$.ajax({
		url:'backend/import_xlsx_to_db.php',
		type: 'POST',
        data:dataString,
		contentType: false,
		processData: false,
		beforeSend: function(){
			 $("#loading").show();
		},
		complete: function(){
			 $("#loading").hide();
		},
		success: function(message){	
			if(message==1){
				$("#error").css('display','none');
				$('input[type=file]').val('');
				$('#success').val('Successfully Added');
				$('.alert').css('display','inline');
				setTimeout(function(){ $('.alert').css('display','none'); }, 2000);
			}
			else if(message=='Family'){

				alert('Dont have Family members details in this excel sheet');
			}
			else{
				$("#error").css('display','inline');
				$("#error_link").attr('href','backend/excel_error_document/'+message);
			}
			
		}
	});
}


  $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })

    //Datemask dd/mm/yyyy
    $('#datemask').inputmask('dd/mm/yyyy', { 'placeholder': 'dd/mm/yyyy' })
    //Datemask2 mm/dd/yyyy
    $('#datemask2').inputmask('mm/dd/yyyy', { 'placeholder': 'mm/dd/yyyy' })
    //Money Euro
    $('[data-mask]').inputmask()

    //Date range picker
    $('#reservationdate').datetimepicker({
        format: 'L'
    });
    //Date range picker
    $('#reservation').daterangepicker()
    //Date range picker with time picker
    $('#reservationtime').daterangepicker({
      timePicker: true,
      timePickerIncrement: 30,
      locale: {
        format: 'MM/DD/YYYY hh:mm A'
      }
    })
    //Date range as a button
    $('#daterange-btn').daterangepicker(
      {
        ranges   : {
          'Today'       : [moment(), moment()],
          'Yesterday'   : [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
          'Last 7 Days' : [moment().subtract(6, 'days'), moment()],
          'Last 30 Days': [moment().subtract(29, 'days'), moment()],
          'This Month'  : [moment().startOf('month'), moment().endOf('month')],
          'Last Month'  : [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
        },
        startDate: moment().subtract(29, 'days'),
        endDate  : moment()
      },
      function (start, end) {
        $('#reportrange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'))
      }
    )

    //Timepicker
    $('#timepicker').datetimepicker({
      format: 'LT'
    })
    
    //Bootstrap Duallistbox
    $('.duallistbox').bootstrapDualListbox()

    //Colorpicker
    $('.my-colorpicker1').colorpicker()
    //color picker with addon
    $('.my-colorpicker2').colorpicker()

    $('.my-colorpicker2').on('colorpickerChange', function(event) {
      $('.my-colorpicker2 .fa-square').css('color', event.color.toString());
    });

    $("input[data-bootstrap-switch]").each(function(){
      $(this).bootstrapSwitch('state', $(this).prop('checked'));
    });

  })
</script>
<script type="text/javascript">
$(document).ready(function () {
  bsCustomFileInput.init();
});
function validation(){
	
	
	if($('#army_no').val()=='' || $('#name').val()=='' || $('#cas_dt').val() =='' ||$('#Salutation').val()=='' || $('#rank').val()==''  ){
		if($('#army_no').val()==''){$('#army_no').css('border','1px solid red')}
		if($('#name').val()==''){$('#name').css('border','1px solid red')}
		if($('#cas_dt').val()==''){$('#cas_dt').css('border','1px solid red')}
		if($('#Salutation').val()==''){$('#Salutation').css('border','1px solid red')}
		if($('#rank').val()==''){$('#rank').css('border','1px solid red')}
		$('#error').html('** Please Fill mandatory  field');
		$('#error').css('display','inline');
		return false;
	}
	
	else{
		$('#error').css('display','none');
		$('input').css('border','1px solid #8080805e');
		return true;
	}
}
</script>


<!-- my date-selector script -->
<script src="./src/gm-date-selector.js"></script>

<!-- my date-selector config -->
<script type="text/javascript">
    dateSelector( '.dateInput');
</script>
</body>
</html>
