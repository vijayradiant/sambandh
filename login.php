<?php 
session_start();
if(isset($_SESSION['name'])){
		header('location:index.php');
}
 INCLUDE('head.php');
?>
<!DOCTYPE html>
<html>
<style>
@media (max-width: 576px){
	.login-box, .register-box {
		margin-top: .5rem;
		 width: 360px; 
	}
}
</style>
<body class="hold-transition login-page" style="background-image: url('img/banner.jpg');">
<div  >
<div class="login-box">
  <div class="login-logo">
  
	
  </div>
  <!-- /.login-logo -->
  <div class="card">
    <div class="card-body login-card-body">
	<div style="text-align:center;">
	<img src="dist/img/logo.png" width="30%"><br>
	</div>
	<p style="color: #d82412;font-weight: 700;text-align: center;font-size: 20px;">Radiant Sambandh</b>
      <p class="login-box-msg">Sign in to start your session</p>
	  <?php if(isset($_GET['error'])){?>
		  <span style="background:#ff00001f;color:red; padding:6px;font-size:13px;">
				** Invalide Username or Password
		  </span>
	  <?php } ?>
      <form action="backend/login_vrification.php" method="post">
        <div class="input-group mb-3">
          <input type="text" class="form-control" name="username" placeholder="Username">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-envelope"></span>
            </div>
          </div>
        </div>
        <div class="input-group mb-3">
          <input type="password" class="form-control" name="password" placeholder="Password">
          <div class="input-group-append">
            <div class="input-group-text">
              <span class="fas fa-lock"></span>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-8">
            <div class="icheck-primary">
              <input type="checkbox" id="remember">
              <label for="remember">
                Remember Me
              </label>
            </div>
          </div>
          <div class="col-4">
            <button type="submit" name="submit" class="btn btn-primary btn-block">Sign In</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
</div>
</body>
</html>
 